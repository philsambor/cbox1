<!--- All methods in this helper will be available in all handlers,views & layouts --->

<cfscript>

function buildTSL() {
        
        return wirebox.getInstance( "TranslationBuilderSVC" );

    }

 function user() {
        
        return wirebox.getInstance( "User" );

    }   

public struct function getTranslations() {    

// Retrieve a bean of the user currently logged in

var userID = auth().getUserID();
var userbean = user().findOrFail(userID);
var userlocale = userbean.getLocaleCode();

// Retrieve the translations based on the user locale

var structRB = structNew();
structRB = buildTSL().getResourceBundles(userlocale);

return structRB;

}   
  

</cfscript>