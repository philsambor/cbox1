<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Tamarind</title>
	<meta name="description" content="ColdBox Application Template">
    <meta name="author" content="Ortus Solutions, Corp">
	
	<!---Base URL --->
	<base href="#event.getHTMLBaseURL()#" />
	
	<!---css --->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50" style="padding-top: 60px">

	<!---Top NavBar --->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" role="navigation">

		<!---Brand --->
		
		<cfif auth().guest()>
		
			<a class="navbar-brand mb-0" href="#event.buildLink('')#"><strong>Home</strong></a>
			
			<cfelse>

			<!--- get translations for the logged-in user --->
			<cfset event.setPrivateValue('tsl', '#getTranslations()#') />

			<a class="navbar-brand mb-0" href="#event.buildLink('')#"><strong>#prc.tsl.menu.HOME_BASE#</strong></a>
		</cfif>
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="##navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<!---About --->
            <ul class="nav navbar-nav ml-auto">

                <cfif auth().guest()>
                    <li class="nav-item">
                        <a href="#event.buildLink( "registrations.new" )#" class="nav-link">Register</a></li>
                    <li class="nav-item">
                        <a href="#event.buildLink( "login" )#" class="nav-link">Log In</a></li>		
                <cfelse>

                    <li class="nav-item">
						<a href="#event.buildLink( "logout" )#" class="nav-link">#prc.tsl.menu.LOGOUT#</a>
					</li>
                </cfif>
			</ul>
		</div>
	</nav> <!---end navbar --->

	<!---Container, Views and Footer --->

	<cfif auth().guest()>
		<div class="container">#renderView()#</div>
		<footer class="border-top py-3 mt-5">
			<div class="container">
				<p class="float-right">
					<a href="##"><i class="fas fa-arrow-up"></i> Back to top</a>
				</p>
				<p>
					<a href="https://www.coldbox.org">ColdBox Platform</a> is a copyright-trademark software by
					<a href="https://www.ortussolutions.com">Ortus Solutions, Corp</a>
				</p>
				<p>
					Design thanks to
					<a href="https://getbootstrap.com/">Twitter Bootstrap</a>
				</p>
			</div>
		</footer>
		<!--- Otherwise, if user is logged-in --->
	<cfelse>
	<!--- Ensure that each event has a default argument value for the selected app --->
		<cfset event.setValue('seqID', '#event.getValue('seqID', 1)#') />
		
		<div class="container-fluid">#runEvent(event='viewlets.sessionview', 
               eventArguments = {seqID=rc.seqID}, prePostExempt=true)#</div>		   
		
		<footer class="border-top py-3 mt-5">
		<div class="container-fluid">
			<p class="float-right">
				<a href="##"><i class="fas fa-arrow-up"></i> Back to top</a>
			</p>
			<div class="d-flex p-3">
				<span class="text-sm flex text-primary">&nbsp;Copyright #year(now())# &copy; Connectrust Pte Ltd.</span>
				<div class="text-sm text-primary">&nbsp;All rights reserved</div>
			</div>
		</div>
	</footer>
	</cfif>    

	<!---js --->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script>
	$(function() {
		// activate all drop downs
		$('.dropdown-toggle').dropdown();
		// Tooltips
		$("[rel=tooltip]").tooltip();
	})
	</script>
</body>
</html>
</cfoutput>
