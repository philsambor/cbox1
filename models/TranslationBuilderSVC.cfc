//  Filename:     TranslationBuilderSVC.cfc
//  Location:	  /models/TranslationBuilderSVC.cfc
//  Created by:   Philippe Sambor
//  Purpose:      I retrieve resource bundles for a locale 

component singleton name="TranslationBuilderSVC" accessors="true"{
	
	// Dependency Injection

	property name="ResourceSVC" inject="resourceService@cbi18n";

	// Retrieve data from Java properties resource bundle files
	
	public struct function getResourceBundles(required string localeCD)	{

	// Given the locale, I load the resource bundles needed for the user session into a structure.
	// There may be more than one resource bundle to load depending whether the translation data is contained
	// in a single or multiple resource files as below (menu, screen and param).

	var structRB = structNew();

	var rb_path = GetDirectoryFromPath(expandpath("/includes/i18n/rbundles/"));
	
	var rbFile1 = "#rb_path#" & "MenuRB";
	var rbFile2 = "#rb_path#" & "ScreenRB";
	var rbFile3 = "#rb_path#" & "ParamRB";

	structRB.menu = ResourceSVC.getResourceBundle(rbFile1,arguments.localeCD);
	structRB.screen = ResourceSVC.getResourceBundle(rbFile2,arguments.localeCD);
	structRB.param = ResourceSVC.getResourceBundle(rbFile3,arguments.localeCD);

	// Translation can be retrieved by appending the dictionary key string to the right of the 
	// structure element, separated by a dot, or by using the getRBstring function from resourceSVC
	// Example for the Cancel button (part of the menuRB bundleCD):
	// rbKey = ResourceSVC.getRBstring("rbFile1","Cancel","myLocale");
	// rbKey = structRB.menu.cancel;

	return structRB;

	}	

}