/**
* I am the MenuContentSVC Model object
* Determines the content of static menu objects that depend on an App being selected
*/

component { 

// Dependency injection

property name="auth" inject="AuthenticationService@cbauth";
property name="buildTSL" inject="models.TranslationBuilderSVC";
property name="_wirebox" inject="wirebox";

public struct function getAuthNavigation()  {

  // Retrieve the user locale

  var userID = auth.getUserID();
  var userbean = _wirebox.getInstance("User").findOrFail(userID);
  var userlocale = userbean.getLocaleCode();

  // Build the menu translations for the user locale

  var structRB = structNew();
  structRB = buildTSL.getResourceBundles(userlocale);
  var tsl = structRB.menu;

  // Build the menu

  var structNav = structNew();
  var navigationArray = arrayNew(2);

  // Define and store the translated top level menu options
  structNav.appsMenuArray = ["#tsl.home_base#","#tsl.administration#","#tsl.Globalization#"];

  // Define and store each application root path
  structNav.appsRootPath = ["", "app_admin", "globalization/home"]

  // Store translated top-level menu optionss in the navigation array

  for (i = 1; i LTE arraylen(structNav.appsMenuArray); i++) {

      navigationArray[i][1] = structNav.appsMenuArray[i];
      navigationArray[i][2] = structNav.appsRootPath[i];		
		 
			}

  // Store sub-level menu option navigation links (must be in english)

   navigationArray[1][3] = ["MyAccount","MySettings"];
   navigationArray[2][3] = ["Processes","Roles","Profiles","User profiles","Profile scopes"];
   navigationArray[3][3] = ["Languages","Locales","Dictionary groups","Dictionary keys","Bundles", "Resource bundles", "Translations"]; 

  // Store translated sub-level menu options

   navigationArray[1][4] = ["#tsl.myAccount#","#tsl.settings#"];
   navigationArray[2][4] = ["#tsl.processes#","#tsl.roles#","#tsl.profiles#","#tsl.assign_profiles#","#tsl.profile_scopes#"];
   navigationArray[3][4] = ["#tsl.languages#","#tsl.locales#","#tsl.dictionary_groups#","#tsl.dictionary_keys#","#tsl.resources#", "#tsl.resource_bundles#", "#tsl.translation#"]; 

  // Create a first array to hold the navigation links
  var optionsMenuArray = arrayNew(1);

  // Create a second one to hold the translated sub-menu options
  var optionsTSLMenuArray = arrayNew(1);

  // Loop through the top-menu options to add each sub-level menu array to a structure 

  for (i = 1; i LTE arraylen(structNav.appsMenuArray); i++) {

      structNav.optionsMenuArray[i] = navigationArray[i][3];
      structNav.optionsTSLMenuArray[i] = navigationArray[i][4];		// Translated submenu options
  
			}   

  // Create an array to hold the menu options URL links
  var optionLink = arrayNew(1);

  // Loop through each application
  for (i = 1; i LTE arraylen(structNav.appsMenuArray); i++) {
    // For each app, associate URL links to its corresponding menu options
    for (j = 1; j LTE arraylen(structNav.optionsMenuArray[i]); j++) {

      // Tweak the menu option name to set it the same as the view handler
      var event_handler = Lcase(ReReplace(structNav.optionsMenuArray[i][j],"[[:space:]]","","ALL"));
      
      // If it exists, extract the Coldbox module name from the apps root path:
      
      var cbmodule = "";
      var delimiter = "/";
      var pathArray = structNav.appsRootPath[i].listToArray(delimiter);

      if (arraylen(pathArray) GT 1) {

        cbmodule = pathArray[1];
        structNav.optionLink[i][j] = cbmodule & "/" & event_handler;

        } else {

        structNav.optionLink[i][j] = event_handler;

      }
          
    }

	}

  return structNav;

  }

// Build the first level menu associated to the currently selected application 
public array function buildAppSubmenu(required numeric seqID) {

// seqID is the sequential order number in which applications are stored in the 
// AppsMenu array.

var arrayAppSubmenu = arraynew(2);

// Get the complete navigation structure
var structNav = getAuthNavigation();

 // Loop through the menu options of the selected application
  
    for (i = 1; i LTE arraylen(structNav.optionsMenuArray[arguments.seqID]); i++) {

      arrayAppSubmenu[arguments.seqID][i][1] = structNav.optionsTSLMenuArray[arguments.seqID][i];
      arrayAppSubmenu[arguments.seqID][i][2] = structNav.optionLink[arguments.seqID][i];

    }

  return arrayAppSubmenu; 

  } 

}