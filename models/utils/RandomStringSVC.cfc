// Filename:     RandomStringSVC.cfc
// Location:     /models/utils/RandomStringSVC.cfc 
// Code from Ben Forta converted from tags to script syntax
// Purpose: Generates a random string

component {  

public string function randstring(required numeric length)  {

  var result = "";

  for (i = 1; i LTE arguments.length; i++) {

      result = result&Chr(RandRange(65, 90));		
		 
			}

    return result;

  }

}