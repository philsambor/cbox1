component extends="tests.resources.BaseIntegrationSpec" {

	property name="buildTSL" inject="models.TranslationBuilderSVC";

	function run(){
		describe( "translation builder service", function() {
			beforeEach( function() {

			} );

			it( "can get resource bundles in Arabic (UAE)", function() {
		
				var RB = buildTSL.getResourceBundles("ar_AE");
				
				expect( RB.menu.home_base ).toBe( "مجلد" );
				expect( RB.menu.myAccount ).toBe( "الحساب الخاص بي" );
				expect( RB.menu.settings ).toBe( "إعدادات" );
				expect( RB.menu.administration ).toBe( "الإدارة" );
				expect( RB.menu.processes ).toBe( "العمليات" );
				expect( RB.menu.roles ).toBe( "الأدوار" );
				expect( RB.menu.profiles ).toBe( "التشكيلات الجانبية" );
				expect( RB.menu.assign_profiles ).toBe( "تعيين ملفات تعريف" );
				expect( RB.menu.profile_scopes ).toBe( "سياقات الشخصية" );
				expect( RB.menu.globalization ).toBe( "التدويل" );
				expect( RB.menu.languages ).toBe( "اللغات" );
				expect( RB.menu.locales ).toBe( "اللغات المحلية" );
				expect( RB.menu.dictionary_groups ).toBe( "قاموس الفصول" );
				expect( RB.menu.dictionary_keys ).toBe( "مفاتيح القاموس" );
				expect( RB.menu.resources ).toBe( "موارد" );
				expect( RB.menu.resource_bundles ).toBe( "حزم الموارد" );
				expect( RB.menu.translation ).toBe( "ترجمة" );
				expect( RB.menu.new ).toBe( "جديد" );
				expect( RB.menu.view ).toBe( "رأي" );
				expect( RB.menu.edit ).toBe( "تحرير" );
				expect( RB.menu.delete ).toBe( "حذف" );
				expect( RB.menu.cancel ).toBe( "إلغاء" );
				expect( RB.menu.save ).toBe( "حفظ" );
				expect( RB.screen.carrier_mode ).toBe( "وضع النقل" );
				expect( RB.param.references_sea ).toBe( "النقل البحري" );

			} );

			it( "can get resource bundles in English (USA)", function() {
	
				var RB = buildTSL.getResourceBundles("en_US");
				
				expect( RB.menu.home_base ).toBe( "my folder" );
				expect( RB.menu.myAccount ).toBe( "my account" );
				expect( RB.menu.settings ).toBe( "settings" );
				expect( RB.menu.administration ).toBe( "administration" );
				expect( RB.menu.processes ).toBe( "processes" );
				expect( RB.menu.roles ).toBe( "roles" );
				expect( RB.menu.profiles ).toBe( "profiles" );
				expect( RB.menu.assign_profiles ).toBe( "assign profiles" );
				expect( RB.menu.profile_scopes ).toBe( "profile scopes" );
				expect( RB.menu.globalization ).toBe( "globalization" );
				expect( RB.menu.languages ).toBe( "languages" );
				expect( RB.menu.locales ).toBe( "locales" );
				expect( RB.menu.dictionary_groups ).toBe( "dictionary groups" );
				expect( RB.menu.dictionary_keys ).toBe( "dictionary keys" );
				expect( RB.menu.resources ).toBe( "resources" );
				expect( RB.menu.resource_bundles ).toBe( "resource bundles" );
				expect( RB.menu.translation ).toBe( "translation" );
				expect( RB.menu.new ).toBe( "new" );
				expect( RB.menu.view ).toBe( "view" );
				expect( RB.menu.edit ).toBe( "edit" );
				expect( RB.menu.delete ).toBe( "delete" );
				expect( RB.menu.cancel ).toBe( "cancel" );
				expect( RB.menu.save ).toBe( "save" );
				expect( RB.screen.carrier_mode ).toBe( "carrier mode" );
				expect( RB.param.references_sea ).toBe( "sea" );

			} );

			it( "can get resource bundles in English (Singapore)", function() {
		
				var RB = buildTSL.getResourceBundles("en_US");
				
				expect( RB.menu.home_base ).toBe( "my folder" );
				expect( RB.menu.myAccount ).toBe( "my account" );
				expect( RB.menu.settings ).toBe( "settings" );
				expect( RB.menu.administration ).toBe( "administration" );
				expect( RB.menu.processes ).toBe( "processes" );
				expect( RB.menu.roles ).toBe( "roles" );
				expect( RB.menu.profiles ).toBe( "profiles" );
				expect( RB.menu.assign_profiles ).toBe( "assign profiles" );
				expect( RB.menu.profile_scopes ).toBe( "profile scopes" );
				expect( RB.menu.globalization ).toBe( "globalization" );
				expect( RB.menu.languages ).toBe( "languages" );
				expect( RB.menu.locales ).toBe( "locales" );
				expect( RB.menu.dictionary_groups ).toBe( "dictionary groups" );
				expect( RB.menu.dictionary_keys ).toBe( "dictionary keys" );
				expect( RB.menu.resources ).toBe( "resources" );
				expect( RB.menu.resource_bundles ).toBe( "resource bundles" );
				expect( RB.menu.translation ).toBe( "translation" );
				expect( RB.menu.new ).toBe( "new" );
				expect( RB.menu.view ).toBe( "view" );
				expect( RB.menu.edit ).toBe( "edit" );
				expect( RB.menu.delete ).toBe( "delete" );
				expect( RB.menu.cancel ).toBe( "cancel" );
				expect( RB.menu.save ).toBe( "save" );
				expect( RB.screen.carrier_mode ).toBe( "carrier mode" );
				expect( RB.param.references_sea ).toBe( "sea" );

			} );


			it( "can get resource bundles in French (France)", function() {
	
				var RB = buildTSL.getResourceBundles("fr_FR");
				
				expect( RB.menu.home_base ).toBe( "mes infos" );
				expect( RB.menu.myAccount ).toBe( "mon compte" );
				expect( RB.menu.settings ).toBe( "configuration" );
				expect( RB.menu.administration ).toBe( "administration" );
				expect( RB.menu.processes ).toBe( "applications" );
				expect( RB.menu.roles ).toBe( "roles" );
				expect( RB.menu.profiles ).toBe( "profils" );
				expect( RB.menu.assign_profiles ).toBe( "attribution des profils" );
				expect( RB.menu.profile_scopes ).toBe( "contextes" );
				expect( RB.menu.globalization ).toBe( "localisation" );
				expect( RB.menu.languages ).toBe( "langues" );
				expect( RB.menu.locales ).toBe( "locales" );
				expect( RB.menu.dictionary_groups ).toBe( "groupes" );
				expect( RB.menu.dictionary_keys ).toBe( "clés" );
				expect( RB.menu.resources ).toBe( "ressources" );
				expect( RB.menu.resource_bundles ).toBe( "localisations" );
				expect( RB.menu.translation ).toBe( "traduction" );
				expect( RB.menu.new ).toBe( "créer" );
				expect( RB.menu.view ).toBe( "voir" );
				expect( RB.menu.edit ).toBe( "modifier" );
				expect( RB.menu.delete ).toBe( "effacer" );
				expect( RB.menu.cancel ).toBe( "annuler" );
				expect( RB.menu.save ).toBe( "enregistrer" );
				expect( RB.screen.carrier_mode ).toBe( "mode de transport" );
				expect( RB.param.references_sea ).toBe( "maritime" );

			} );

			it( "can get resource bundles in French (Canada)", function() {
		
				var RB = buildTSL.getResourceBundles("fr_CA");
				
				expect( RB.menu.home_base ).toBe( "mes infos" );
				expect( RB.menu.myAccount ).toBe( "mon compte" );
				expect( RB.menu.settings ).toBe( "configuration" );
				expect( RB.menu.administration ).toBe( "administration" );
				expect( RB.menu.processes ).toBe( "applications" );
				expect( RB.menu.roles ).toBe( "roles" );
				expect( RB.menu.profiles ).toBe( "profils" );
				expect( RB.menu.assign_profiles ).toBe( "attribution des profils" );
				expect( RB.menu.profile_scopes ).toBe( "contextes" );
				expect( RB.menu.globalization ).toBe( "localisation" );
				expect( RB.menu.languages ).toBe( "langues" );
				expect( RB.menu.locales ).toBe( "locales" );
				expect( RB.menu.dictionary_groups ).toBe( "groupes" );
				expect( RB.menu.dictionary_keys ).toBe( "clés" );
				expect( RB.menu.resources ).toBe( "ressources" );
				expect( RB.menu.resource_bundles ).toBe( "localisations" );
				expect( RB.menu.translation ).toBe( "traduction" );
				expect( RB.menu.new ).toBe( "créer" );
				expect( RB.menu.view ).toBe( "voir" );
				expect( RB.menu.edit ).toBe( "modifier" );
				expect( RB.menu.delete ).toBe( "effacer" );
				expect( RB.menu.cancel ).toBe( "annuler" );
				expect( RB.menu.save ).toBe( "enregistrer" );
				expect( RB.screen.carrier_mode ).toBe( "mode de transport" );
				expect( RB.param.references_sea ).toBe( "maritime" );

			} );

		it( "can get resource bundles in Japanese", function() {
		
				var RB = buildTSL.getResourceBundles("ja_JP");
				
				expect( RB.menu.home_base ).toBe( "私のフォルダー" );
				expect( RB.menu.myAccount ).toBe( "私のアカウント" );
				expect( RB.menu.settings ).toBe( "設定" );
				expect( RB.menu.administration ).toBe( "管理" );
				expect( RB.menu.processes ).toBe( "アプリケーション" );
				expect( RB.menu.roles ).toBe( "ロール" );
				expect( RB.menu.profiles ).toBe( "プロファイル" );
				expect( RB.menu.assign_profiles ).toBe( "プロファイルを割り当てください。" );
				expect( RB.menu.profile_scopes ).toBe( "コンテキスト" );
				expect( RB.menu.globalization ).toBe( "グローバル化" );
				expect( RB.menu.languages ).toBe( "言語" );
				expect( RB.menu.locales ).toBe( "ロケール" );
				expect( RB.menu.dictionary_groups ).toBe( "辞書の章" );
				expect( RB.menu.dictionary_keys ).toBe( "ディクショナリ内のキー" );
				expect( RB.menu.resources ).toBe( "リソース" );
				expect( RB.menu.resource_bundles ).toBe( "バンドル" );
				expect( RB.menu.translation ).toBe( "翻訳" );
				expect( RB.menu.new ).toBe( "新しい" );
				expect( RB.menu.view ).toBe( "ビュー" );
				expect( RB.menu.edit ).toBe( "編集" );
				expect( RB.menu.delete ).toBe( "削除します" );
				expect( RB.menu.cancel ).toBe( "キャンセル" );
				expect( RB.menu.save ).toBe( "保存" );
				expect( RB.screen.carrier_mode ).toBe( "輸送モード" );
				expect( RB.param.references_sea ).toBe( "海上輸送" );

			} );

			it( "can get resource bundles in Korean", function() {
		
				var RB = buildTSL.getResourceBundles("ko_KR");
				
				expect( RB.menu.home_base ).toBe( "내 폴더" );
				expect( RB.menu.myAccount ).toBe( "내 계정" );
				expect( RB.menu.settings ).toBe( "설정" );
				expect( RB.menu.administration ).toBe( "관리" );
				expect( RB.menu.processes ).toBe( "응용 프로그램" );
				expect( RB.menu.roles ).toBe( "역할" );
				expect( RB.menu.profiles ).toBe( "프로 파일" );
				expect( RB.menu.assign_profiles ).toBe( "프로필 할당" );
				expect( RB.menu.profile_scopes ).toBe( "컨텍스트" );
				expect( RB.menu.globalization ).toBe( "세계화" );
				expect( RB.menu.languages ).toBe( "언어 선택" );
				expect( RB.menu.locales ).toBe( "로케일" );
				expect( RB.menu.dictionary_groups ).toBe( "사전 장" );
				expect( RB.menu.dictionary_keys ).toBe( "사전 키" );
				expect( RB.menu.resources ).toBe( "리소스" );
				expect( RB.menu.resource_bundles ).toBe( "번역 번들" );
				expect( RB.menu.translation ).toBe( "번역" );
				expect( RB.menu.new ).toBe( "새로운" );
				expect( RB.menu.view ).toBe( "전망" );
				expect( RB.menu.edit ).toBe( "편집" );
				expect( RB.menu.delete ).toBe( "삭제" );
				expect( RB.menu.cancel ).toBe( "취소" );
				expect( RB.menu.save ).toBe( "저장" );
				expect( RB.screen.carrier_mode ).toBe( "운송 모드" );
				expect( RB.param.references_sea ).toBe( "바다 수송" );

			} );

			it( "can get resource bundles in Thai", function() {
		
				var RB = buildTSL.getResourceBundles("th_TH");
				
				expect( RB.menu.home_base ).toBe( "โฟลเดอร์ของฉัน" );
				expect( RB.menu.myAccount ).toBe( "บัญชีของฉัน" );
				expect( RB.menu.settings ).toBe( "การตั้งค่า" );
				expect( RB.menu.administration ).toBe( "จัดการ" );
				expect( RB.menu.processes ).toBe( "การใช้งาน" );
				expect( RB.menu.roles ).toBe( "บทบาท" );
				expect( RB.menu.profiles ).toBe( "ส่วนกำหนดค่า" );
				expect( RB.menu.assign_profiles ).toBe( "กำหนดโพรไฟล์" );
				expect( RB.menu.profile_scopes ).toBe( "บริบท" );
				expect( RB.menu.globalization ).toBe( "โลกาภิวัฒน์" );
				expect( RB.menu.languages ).toBe( "ภาษา" );
				expect( RB.menu.locales ).toBe( "ตำแหน่งที่ตั้ง" );
				expect( RB.menu.dictionary_groups ).toBe( "บทพจนานุกรม" );
				expect( RB.menu.dictionary_keys ).toBe( "คีย์ในพจนานุกรม" );
				expect( RB.menu.resources ).toBe( "ทรัพยากร" );
				expect( RB.menu.resource_bundles ).toBe( "การรวมกลุ่ม" );
				expect( RB.menu.translation ).toBe( "แปล" );
				expect( RB.menu.new ).toBe( "สร้าง" );
				expect( RB.menu.view ).toBe( "ดู" );
				expect( RB.menu.edit ).toBe( "แก้ไข" );
				expect( RB.menu.delete ).toBe( "ลบ" );
				expect( RB.menu.cancel ).toBe( "ยกเลิก" );
				expect( RB.menu.save ).toBe( "ประหยัด" );
				expect( RB.screen.carrier_mode ).toBe( "โหมดการขนส่ง" );
				expect( RB.param.references_sea ).toBe( "ขนส่งทางทะเล" );

			} );

			it( "can get resource bundles in Chinese (Singapore)", function() {
		
				var RB = buildTSL.getResourceBundles("zh_SG");
				
				expect( RB.menu.home_base ).toBe( "我的文件夹" );
				expect( RB.menu.myAccount ).toBe( "我的账户" );
				expect( RB.menu.settings ).toBe( "设置" );
				expect( RB.menu.administration ).toBe( "管理" );
				expect( RB.menu.processes ).toBe( "应用程序" );
				expect( RB.menu.roles ).toBe( "角色" );
				expect( RB.menu.profiles ).toBe( "配置文件" );
				expect( RB.menu.assign_profiles ).toBe( "分配配置文件" );
				expect( RB.menu.profile_scopes ).toBe( "上下文" );
				expect( RB.menu.globalization ).toBe( "全球化" );
				expect( RB.menu.languages ).toBe( "语言" );
				expect( RB.menu.locales ).toBe( "语言区域设置" );
				expect( RB.menu.dictionary_groups ).toBe( "字典的章节" );
				expect( RB.menu.dictionary_keys ).toBe( "字典的键" );
				expect( RB.menu.resources ).toBe( "资源" );
				expect( RB.menu.resource_bundles ).toBe( "翻译捆绑" );
				expect( RB.menu.translation ).toBe( "翻译" );
				expect( RB.menu.new ).toBe( "创建" );
				expect( RB.menu.view ).toBe( "查看" );
				expect( RB.menu.edit ).toBe( "编辑" );
				expect( RB.menu.delete ).toBe( "删除" );
				expect( RB.menu.cancel ).toBe( "取消" );
				expect( RB.menu.save ).toBe( "节省" );
				expect( RB.screen.carrier_mode ).toBe( "运输方式" );
				expect( RB.param.references_sea ).toBe( "海上运输" );

			} );

		} );
	}

}
