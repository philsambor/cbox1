component extends="tests.resources.BaseIntegrationSpec" {

	property name="menu" inject="models.MenuContentSVC";
	property name="auth" inject="AuthenticationService@cbauth";
	property name="buildTSL" inject="models.TranslationBuilderSVC";

	function run(){
		describe( "menu content service", function() {
			beforeEach( function() {

			} );

			it( "can generate a navigation structure", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );
				
				var locale = "ja_JP"

				var structRB = structNew();
  				structRB = buildTSL.getResourceBundles(locale);
  				var tsl = structRB.menu;

				var testArray = arrayNew();  

				testArray[1] = ["#tsl.myAccount#","#tsl.settings#"];
				testArray[2] = ["#tsl.processes#","#tsl.roles#","#tsl.profiles#","#tsl.assign_profiles#","#tsl.profile_scopes#"];
   				testArray[3] = ["#tsl.languages#","#tsl.locales#","#tsl.dictionary_groups#","#tsl.dictionary_keys#","#tsl.resources#", "#tsl.resource_bundles#", "#tsl.translation#"];  				

				debug(testArray);

			} );

			it( "can generate an application submenu array", function() {

			// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });	

			// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );	
	
				var Home = menu.buildAppSubmenu(1);
				var Admin = menu.buildAppSubmenu(2);
				var Globalization = menu.buildAppSubmenu(3);

				expect(Home[1][1][2]).toBe("myAccount");
				expect(Admin[2][4][2]).toBe("userprofiles");
				expect(Globalization[3][6][2]).toBe("globalization/resourcebundles");

			} );

		} );
	}

}
