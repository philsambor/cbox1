component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "bundles", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "Bundle" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new bundle", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Bundle" ).count() ).toBe( prc.count, "bundles should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/globalization/bundles?seqID=3", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "Bundle" ).count() ).toBe( prc.count+1 , "One more bundle should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Bundle created successfully!" );

			} );

			it( "can update a bundle", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var Bundle = getInstance( "Bundle" ).create( prc.data );
				var CD = Bundle.getBundleCD();

				// one new record should have been added to the database
				expect( getInstance( "Bundle" ).count() ).toBe( prc.count+1 , "One more bundle should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/globalization/bundles/#CD#?seqID=3", validParams({ "BundleName" : "UPDATED NAME" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedBundle = getInstance( "Bundle" ).findOrFail(CD);
				expect( updatedBundle.getBundleName()).toBe("UPDATED NAME");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Bundle updated successfully!" );

			} );

			it( "Create or update require a valid bundle name", function() {

				expect( getInstance( "Bundle" ).count() ).toBe( prc.count, "bundles already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/bundles?seqID=3", validParams( { "BundleName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "Bundle_form_errors" );
				expect( errors.Bundle_form_errors ).toHaveKey( "BundleName" );

				// No record was added because of the validation failure
				expect( getInstance( "Bundle" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the BundleCD = "menuRB" exists in the database
				expect( getInstance( "Bundle" ).existsOrFail("menuRB") ).toBe(true);

				var event = put( "/globalization/bundles/menuRB?seqID=3", validParams( { "BundleName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "Bundle_form_errors" );
				expect( errors.Bundle_form_errors ).toHaveKey( "BundleName" );


			} );

			it( "Create or update require a valid bundle extranet name", function() {

				expect( getInstance( "Bundle" ).count() ).toBe( prc.count, "bundles already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/bundles?seqID=3", validParams( { "BundleExtranetName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "Bundle_form_errors" );
				expect( errors.Bundle_form_errors ).toHaveKey( "BundleExtranetName" );

				// No record was added because of the validation failure
				expect( getInstance( "Bundle" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the BundleCD = "menuRB" exists in the database
				expect( getInstance( "Bundle" ).existsOrFail("menuRB") ).toBe(true);

				var event = put( "/globalization/bundles/menuRB?seqID=3", validParams( { "BundleExtranetName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "Bundle_form_errors" );
				expect( errors.Bundle_form_errors ).toHaveKey( "BundleExtranetName" );


			} );

			it( "cannot delete a constrained bundle", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record RessourceCD = "menuRB" is constrained by a child entity, it cannot be deleted
				var event = delete( "/globalization/bundles/menuRB?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "Bundle" )
				.where( "BundleCD","=", "menuRB" )
				.count();

				// Record is still there
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a bundle when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new bundle for testing

				var Bundle = getInstance( "Bundle" ).create( prc.data );
				var CD = Bundle.getBundleCD();

				// This record is not contrained and can be deleted
				var event = delete( "/globalization/bundles/#CD#?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "Bundle" )
				.where( "BundleCD","=", CD )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Bundle deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "BundleCD" : "testRB",  
					 "BundleName" : "TEST",
					 "BundleExtranetName" : "GLOBAL-G2G" };

		structAppend( data, arguments.overrides, true );
		return data;
	}

}
