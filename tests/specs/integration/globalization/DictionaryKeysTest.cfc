component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "dictionaryKeys", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "DictionaryKey" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new Dictionary key", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "DictionaryKey" ).count() ).toBe( prc.count, "dictionary keys should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/globalization/dictionaryKeys?seqID=3", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "DictionaryKey" ).count() ).toBe( prc.count+1 , "One more DictionaryKey should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Dictionary key created successfully!" );

			} );

			it( "can update a Dictionary Key", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var dictionaryKey = getInstance( "DictionaryKey" ).create( prc.data );
				var str = dictionaryKey.getDictionaryKeyString();

				// one new record should have been added to the database
				expect( getInstance( "DictionaryKey" ).count() ).toBe( prc.count+1 , "One more Dictionary key should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/globalization/dictionaryKeys/#str#?seqID=3", validParams({ "DictionaryKeyBundleCD" : "menuRB",
																							  "DictionaryKeyGroupID" : 3 }) );

				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedDictionaryKey = getInstance( "DictionaryKey" ).findOrFail(str);
				expect( updatedDictionaryKey.getDictionaryKeyBundleCD()).toBe("menuRB");
				expect( updatedDictionaryKey.getDictionaryKeyGroupID()).toBe(3);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Dictionary key updated successfully!" );

			} );

			it( "Create or update require a valid Dictionary key source translation", function() {

				expect( getInstance( "DictionaryKey" ).count() ).toBe( prc.count, "dictionary keys already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/dictionaryKeys?seqID=3", validParams( { "DictionaryKeySourceTranslation" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "DictionaryKey_form_errors" );
				expect( errors.DictionaryKey_form_errors ).toHaveKey( "DictionaryKeySourceTranslation" );

				// No record was added because of the validation failure
				expect( getInstance( "DictionaryKey" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the DictionaryKeyString = "ACCOUNTING" exists in the database
				expect( getInstance( "DictionaryKey" ).existsOrFail("ACCOUNTING") ).toBe(true);

				var event = put( "/globalization/dictionaryKeys/ACCOUNTING?seqID=3", validParams( { "DictionaryKeySourceTranslation" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "DictionaryKey_form_errors" );
				expect( errors.DictionaryKey_form_errors ).toHaveKey( "DictionaryKeySourceTranslation" );


			} );

			it( "Create or update require a unique dictionary UI key", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "DictionaryKey" ).count() ).toBe( prc.count, "Dictionary keys should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Upon CREATE

				var event = post( "/globalization/dictionaryKeys?seqID=3", validParams( { "DictionaryUIKey" : "Logistics" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "DictionaryKey_form_errors" );
				expect( errors.DictionaryKey_form_errors ).toHaveKey( "DictionaryUIKey" );

				// No record was added because of the validation failure
				expect( getInstance( "DictionaryKey" ).count() ).toBe( prc.count , "No Dictionary key should have been added to the database" );

				// Upon UPDATE

				// Make sure the DictionaryKeyString = "ACCOUNTING" exists in the database
				expect( getInstance( "DictionaryKey" ).existsOrFail("ACCOUNTING") ).toBe(true);

				var event = put( "/globalization/dictionaryKeys/ACCOUNTING?seqID=3", validParams( { "DictionaryUIKey" : "Logistics" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// A database duplicate index error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Unique index or primary key violation" );

			} );

			it( "cannot delete a contrained Dictionary key", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// A dictionary key maybe twice constrained if it is used to define a process:
				// DELETE RESTRICT on Process and DELETE CASCADE on Translation

				// we need to create a translation record associated to an existing dictionary key
				var translation = getInstance( "Translation" ).create( { "translationBundleName" : "menuRB_fr_FR",
									   	 								 "translationDictionaryKey" : "ACCOUNTING",
					                   	 								 "translationTranslatedKey" : "comptabilité",
										 								 "translationTranslatedAscii" : "compta",	
										 								 "translationStatus" : 1
														 				});

				var ID = translation.getTranslationID();

				// record DictionaryKeyString = "ACCOUNTING" has a child record in the process entity
				// If I attempt to delete this dictionary key, a foreign key violation will be raised
				
				var event = delete( "/globalization/dictionaryKeys/ACCOUNTING?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				var isDictionaryKeyDeleted = getInstance( "DictionaryKey" )
				.where( "DictionaryKeyString","=", "ACCOUNTING" )
				.count();

				// var isTranslationDeleted = getInstance( "Translation" )
				// .where( "translationID","=", ID )
				// .count();

				// The dictionary was not deleted because it is constrained in the "Process" entity
				expect(isDictionaryKeyDeleted).toBe(1);
				//expect(isTranslationDeleted).toBe(0);

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );
	
			} );

			it( "will delete CASCADE on Translation when Dictionary key is not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new, but un-constrained dictionary key

				var DictionaryKey = getInstance( "DictionaryKey" ).create( prc.data );
				var CD = DictionaryKey.getDictionaryKeyString();

				// Let's create a translation record associated to this new dictionary key
				var translation = getInstance( "Translation" ).create( { "translationBundleName" : "screenRB_fr_FR",
									   	 								 "translationDictionaryKey" : "ARTWORK",
					                   	 								 "translationTranslatedKey" : "oeuvre d'art",
										 								 "translationTranslatedAscii" : "",	
										 								 "translationStatus" : 1
														 				});

				var ID = translation.getTranslationID();

				// This record is not contrained and can be deleted
				var event = delete( "/globalization/dictionaryKeys/#CD#?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// Dictionary key should no longer be found in the database
				var isDictionaryKeyDeleted = getInstance( "DictionaryKey" )
				.where( "DictionaryKeyString","=", CD )
				.count();

				// Dictionary key record is no longer in the database
				expect(isDictionaryKeyDeleted).toBe(0);

				// DELETE should have cascaded to the associated translation record
				var isTranslationDeleted = getInstance( "Translation" )
				.where( "translationID","=", ID )
				.count();

				// Associated translation is also gone
				expect(isTranslationDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Dictionary key deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "DictionaryKeyString" : "artwork",  
					 "dictionaryKeyBundleCD" : "screenRB",
					 "dictionaryKeySourceLanguageCD" : "EN",
					 "dictionaryKeyGroupID" : 1,
					 "dictionaryKeySourceTranslation" : "art work",
					 "dictionaryUIKey" : "Artwork"
					  };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
