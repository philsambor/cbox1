component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "languages", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "Language" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new language", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Language" ).count() ).toBe( prc.count, "Languages should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/globalization/languages?seqID=3", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "Language" ).count() ).toBe( prc.count+1 , "One more language should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Language created successfully!" );

			} );

			it( "can update a language", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var language = getInstance( "Language" ).create( prc.data );
				var CD = language.getLanguageCD();

				// one new record should have been added to the database
				expect( getInstance( "Language" ).count() ).toBe( prc.count+1 , "One more language should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/globalization/languages/#CD#?seqID=3", validParams({ "languageName" : "GAELIC" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedLanguage = getInstance( "Language" ).findOrFail(CD);
				expect( updatedLanguage.getLanguageName()).toBe("GAELIC");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Language updated successfully!" );

			} );

			it( "Create or update require a unique language name", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Language" ).count() ).toBe( prc.count, "Languages should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Upon CREATE

				var event = post( "/globalization/languages?seqID=3", validParams( { "languageName" : "ENGLISH" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "language_form_errors" );
				expect( errors.language_form_errors ).toHaveKey( "languageName" );

				// No record was added because of the validation failure
				expect( getInstance( "Language" ).count() ).toBe( prc.count , "No language should have been added to the database" );

				// Upon UPDATE

				// Make sure the languageCD = "FR" exists in the database
				expect( getInstance( "Language" ).existsOrFail("FR") ).toBe(true);

				var event = put( "globalization/languages/FR?seqID=3", validParams( { "languageName" : "ENGLISH" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// A database duplicate index error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Unique index or primary key violation" );

			} );

			it( "Create or update require a valid language name", function() {

				expect( getInstance( "Language" ).count() ).toBe( prc.count, "Languages already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/languages?seqID=3", validParams( { "languageName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "language_form_errors" );
				expect( errors.language_form_errors ).toHaveKey( "languageName" );

				// No record was added because of the validation failure
				expect( getInstance( "Language" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the languageCD = "TH" exists in the database
				expect( getInstance( "Language" ).existsOrFail("TH") ).toBe(true);

				var event = put( "/globalization/languages/TH?seqID=3", validParams( { "languageName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "language_form_errors" );
				expect( errors.language_form_errors ).toHaveKey( "languageName" );


			} );

			it( "cannot delete a constrained language", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record languageCD = "FR" is constrained by a child entity, it cannot be deleted
				var event = delete( "/globalization/languages/FR?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "Language" )
				.where( "languageCD","=", "FR" )
				.count();

				// Record is still there
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a language when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new language for testing

				var language = getInstance( "Language" ).create( prc.data );
				var CD = language.getLanguageCD();

				// This record is not contrained and can be deleted
				var event = delete( "/globalization/languages/#CD#?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "Language" )
				.where( "languageCD","=", CD )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Language deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "languageCD" : "ET",  
					 "languageName" : "ESTONIAN" };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
