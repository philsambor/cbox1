component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "translations", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "Translation" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new translation", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Translation" ).count() ).toBe( prc.count, "There are no translations in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/globalization/translations?seqID=3", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "Translation" ).count() ).toBe( prc.count+1 , "One more translation should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Translation created successfully!" );

			} );

			it( "can update a translation", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var translation = getInstance( "Translation" ).create( prc.data );
				var ID = translation.getTranslationID();

				// one new record should have been added to the database
				expect( getInstance( "Translation" ).count() ).toBe( prc.count+1 , "One more translation should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/globalization/translations/#ID#?seqID=3", validParams({ "TranslationStatus" : 0 }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedTranslation = getInstance( "Translation" ).findOrFail(ID);
				expect( updatedTranslation.getTranslationDictionaryKey()).toBe("ACCOUNTING");
				expect( updatedTranslation.getTranslationStatus()).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Translation updated successfully!" );

			} );

			it( "can delete any translation", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new translation for testing

				var translation = getInstance( "Translation" ).create( prc.data );
				var ID = translation.getTranslationID();

				// No translations are contrained with foreign keys and can be deleted
				var event = delete( "/globalization/translations/#ID#?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "Translation" )
				.where( "TranslationID","=", ID )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Translation deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "translationBundleName" : "menuRB_fr_FR",
					 "translationDictionaryKey" : "ACCOUNTING",
					 "translationTranslatedKey" : "comptabilité",
					 "translationTranslatedAscii" : "compta",	
					 "translationStatus" : 1 };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
