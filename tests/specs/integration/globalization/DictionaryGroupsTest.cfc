component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "dictionary groups", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "DictionaryGroup" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new dictionary group", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count, "Dictionary groups should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/globalization/dictionaryGroups?seqID=3", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count+1 , "One more dictionary group should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Dictionary group created successfully!" );

			} );

			it( "can update a dictionary group", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var dictionaryGroup = getInstance( "DictionaryGroup" ).create( prc.data );
				var ID = dictionaryGroup.getDictionaryGroupID();

				// one new record should have been added to the database
				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count+1 , "One more dictionary group should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/globalization/dictionaryGroups/#ID#?seqID=3", validParams({ "dictionaryGroupName" : "UPDATED GROUP NAME" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedDictionaryGroup = getInstance( "DictionaryGroup" ).findOrFail(ID);
				expect( updatedDictionaryGroup.getDictionaryGroupName()).toBe("UPDATED GROUP NAME");
				expect( updatedDictionaryGroup.getDictionaryGroupDescription()).toBe("TEST GROUP");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Dictionary group updated successfully!" );

			} );

			it( "Create or update require a unique dictionary group name", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count, "Dictionary groups should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Upon CREATE

				var event = post( "/globalization/dictionaryGroups?seqID=3", validParams( { "dictionaryGroupName" : "NOGROUP" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "dictionaryGroup_form_errors" );
				expect( errors.dictionaryGroup_form_errors ).toHaveKey( "dictionaryGroupName" );

				// No record was added because of the validation failure
				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count , "No dictionary group should have been added to the database" );

				// Upon UPDATE

				// Make sure the dictionaryGroupID = 3 exists in the database
				expect( getInstance( "DictionaryGroup" ).existsOrFail(3) ).toBe(true);

				var event = put( "/globalization/dictionaryGroups/3?seqID=3", validParams( { "dictionaryGroupName" : "NOGROUP" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// A database duplicate index error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Unique index or primary key violation" );

			} );

			it( "Create or update require a valid dictionary group name", function() {

				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count, "Dictionary groups already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/dictionaryGroups?seqID=3", validParams( { "dictionaryGroupName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "dictionaryGroup_form_errors" );
				expect( errors.dictionaryGroup_form_errors ).toHaveKey( "dictionaryGroupName" );

				// No record was added because of the validation failure
				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the dictionaryGroupID = 3 exists in the database
				expect( getInstance( "DictionaryGroup" ).existsOrFail(3) ).toBe(true);

				var event = put( "/globalization/dictionaryGroups/3?seqID=3", validParams( { "dictionaryGroupName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "dictionaryGroup_form_errors" );
				expect( errors.dictionaryGroup_form_errors ).toHaveKey( "dictionaryGroupName" );


			} );

			it( "Create or update require a valid dictionary group description", function() {

				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count, "Dictionary groups already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/dictionaryGroups?seqID=3", validParams( { "dictionaryGroupDescription" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "dictionaryGroup_form_errors" );
				expect( errors.dictionaryGroup_form_errors ).toHaveKey( "dictionaryGroupDescription" );

				// No record was added because of the validation failure
				expect( getInstance( "DictionaryGroup" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the dictionaryGroupID = 3 exists in the database
				expect( getInstance( "DictionaryGroup" ).existsOrFail(3) ).toBe(true);

				var event = put( "/globalization/dictionaryGroups/3?seqID=3", validParams( { "dictionaryGroupDescription" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "dictionaryGroup_form_errors" );
				expect( errors.dictionaryGroup_form_errors ).toHaveKey( "dictionaryGroupDescription" );

			} );

			it( "cannot delete a constrained dictionary group", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record ID = 3 is constrained by a child entity, it cannot be deleted
				var event = delete( "/globalization/dictionaryGroups/3?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "DictionaryGroup" )
				.where( "dictionaryGroupID","=", 3 )
				.count();

				// Record is still there
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a dictionary group when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new dictionary group for testing

				var dictionaryGroup = getInstance( "DictionaryGroup" ).create( prc.data );
				var ID = dictionaryGroup.getDictionaryGroupID();

				// This record ID is not contrained and can be deleted
				var event = delete( "/globalization/dictionaryGroups/#ID#?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "DictionaryGroup" )
				.where( "dictionaryGroupID","=", ID )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Dictionary group deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "dictionaryGroupName" : "TEST",  
					 "dictionaryGroupDescription" : "TEST GROUP" };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
