component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "locales", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "Locale" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new locale", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Locale" ).count() ).toBe( prc.count, "Locales should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/globalization/locales?seqID=3", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "Locale" ).count() ).toBe( prc.count+1 , "One more locale should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Locale created successfully!" );

			} );

			it( "can update a locale", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var locale = getInstance( "Locale" ).create( prc.data );
				var CD = locale.getLocaleCD();

				// one new record should have been added to the database
				expect( getInstance( "Locale" ).count() ).toBe( prc.count+1 , "One more locale should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/globalization/locales/#CD#?seqID=3", validParams({ "localeLanguageCD" : "IT",
															    					  "localeName" : "ITALIAN" } ));

				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedLocale = getInstance( "Locale" ).findOrFail(CD);

				expect( updatedLocale.getLocaleLanguageCD() ).toBe("IT");
				expect( updatedLocale.getLocaleName() ).toBe("ITALIAN");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Locale updated successfully!" );

			} );

			it( "Create or update require a valid locale name", function() {

				expect( getInstance( "Locale" ).count() ).toBe( prc.count, "Locales already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/locales?seqID=3", validParams( { "localeName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "locale_form_errors" );
				expect( errors.locale_form_errors ).toHaveKey( "localeName" );

				// No record was added because of the validation failure
				expect( getInstance( "Locale" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the localeCD = "fr_FR" exists in the database
				expect( getInstance( "Locale" ).existsOrFail("fr_FR") ).toBe(true);

				var event = put( "/globalization/locales/fr_FR?seqID=3", validParams( { "localeName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "locale_form_errors" );
				expect( errors.locale_form_errors ).toHaveKey( "localeName" );

			} );

			it( "Create or update require a valid calendar language", function() {

				expect( getInstance( "Locale" ).count() ).toBe( prc.count, "Locales already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/globalization/locales?seqID=3", validParams( { "calendarLang" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "locale_form_errors" );
				expect( errors.locale_form_errors ).toHaveKey( "calendarLang" );

				// No record was added because of the validation failure
				expect( getInstance( "Locale" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the localeCD = "fr_FR" exists in the database
				expect( getInstance( "Locale" ).existsOrFail("fr_FR") ).toBe(true);

				var event = put( "/globalization/locales/fr_FR?seqID=3", validParams( { "calendarLang" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "locale_form_errors" );
				expect( errors.locale_form_errors ).toHaveKey( "calendarLang" );

			} );

			it( "cannot delete a constrained locale", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record localeCD = "fr_FR" is constrained by a child entity, it cannot be deleted
				var event = delete( "/globalization/locales/fr_FR?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "Locale" )
				.where( "localeCD","=", "fr_FR" )
				.count();

				// Record is still there
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a locale when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new language for testing

				var locale = getInstance( "Locale" ).create( prc.data );
				var CD = locale.getLocaleCD();

				// This record is not contrained and can be deleted
				var event = delete( "/globalization/locales/#CD#?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "Locale" )
				.where( "localeCD","=", CD )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Locale deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "localeCD" : "fr_MA", 
					 "localeLanguageCD" : "FR", 
					 "localeName" : "FRENCH (MAROC)",
					 "calendarLang" : "fr",
					 "isGregorianCalendar" : 1 };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
