component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "resource bundles", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "ResourceBundle" ).count(); 
			prc.data = validParams();
				

			} );

			it( "can create a new resource bundle", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "ResourceBundle" ).count() ).toBe( prc.count, "resource bundles should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// First, we must create a new bundle "testRB" (needed as a foreign key)
				post( "/globalization/bundles?seqID=3", { "bundleCD" : "testRB",
												  		  "bundleName" : "TEST",
					                              		  "bundleExtranetName" : "GLOBAL-G2G" } );

				// Ensure that the new bundle exists
				expect( getInstance( "Bundle" ).existsOrFail("testRB") ).toBe(true);					   

				// Create a new resource bundle record and verify that the CREATE action is actually called 
				var event = post( "/globalization/resourceBundles?seqID=3", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "ResourceBundle" ).count() ).toBe( prc.count+1 , "One more resource bundle should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Resource bundle created successfully!" );

			} );

			it( "can update a resource bundle", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// First create a testRB bundle (needed as foreign key)
				post( "/globalization/bundles?seqID=3", { "bundleCD" : "testRB",
														  "bundleName" : "TEST",
					                					  "bundleExtranetName" : "GLOBAL-G2G" } );

				// Create a new resource bundle record
				var resourceBundle = getInstance( "ResourceBundle" ).create( prc.data );
				var Name = ResourceBundle.getResourceBundleName();

				// one new record should have been added to the database
				expect( getInstance( "ResourceBundle" ).count() ).toBe( prc.count+1 , "One more Resource should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/globalization/resourceBundles/#Name#?seqID=3", validParams({ "ResourceBundleComment" : "UPDATED COMMENT" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedResourceBundle = getInstance( "ResourceBundle" ).findOrFail(Name);
				expect( updatedResourceBundle.getResourceBundleComment()).toBe("UPDATED COMMENT");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Resource bundle updated successfully!" );

			} );

			it( "Create or update require a valid Resource bundle comment", function() {

				expect( getInstance( "ResourceBundle" ).count() ).toBe( prc.count, "resources already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// First, we must create a new bundle "testRB" (needed as a foreign key)
				post( "/globalization/bundles?seqID=3", { "bundleCD" : "testRB",
														  "bundleName" : "TEST",
					                					  "bundleExtranetName" : "GLOBAL-G2G" } );

				// Ensure that the new bundle exists
				expect( getInstance( "Bundle" ).existsOrFail("testRB") ).toBe(true);	
				
				// upon CREATE

				var event = post( "/globalization/resourceBundles?seqID=3", validParams( { "ResourceBundleComment" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "ResourceBundle_form_errors" );
				expect( errors.ResourceBundle_form_errors ).toHaveKey( "ResourceBundleComment" );

				// No record was added because of the validation failure
				expect( getInstance( "ResourceBundle" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the ResourceBundleCD = "menuRB_en_US" exists in the database
				var RBname = "menuRB_en_US"
				expect( getInstance( "ResourceBundle" ).existsOrFail("#RBname#") ).toBe(true);

				var event = put( "/globalization/resourceBundles/#RBname#?seqID=3", validParams( { "ResourceBundleComment" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "ResourceBundle_form_errors" );
				expect( errors.ResourceBundle_form_errors ).toHaveKey( "ResourceBundleComment" );


			} );

			it( "cannot delete a constrained resource bundle", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// we need to create a translation record to constrain a resource bundle
				var translation = getInstance( "Translation" ).create( { "translationBundleName" : "menuRB_fr_FR",
									   	 								 "translationDictionaryKey" : "ACCOUNTING",
					                   	 								 "translationTranslatedKey" : "comptabilité",
										 								 "translationTranslatedAscii" : "compta",	
										 								 "translationStatus" : 1
														 				});

				var ID = translation.getTranslationID();

				// Ensure that the translation record exists
				expect( getInstance( "Translation" ).existsOrFail(ID) ).toBe(true);						 

				// record ResourceBundleCD = "menuRB_fr_FR" is now constrained by a child entity (translation),
				// but can be deleted while deleting all associated translations:

				var event = delete( "/globalization/resourceBundles/menuRB_fr_FR?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "ResourceBundle" )
				.where( "ResourceBundleName","=", "menuRB_fr_FR" )
				.count();

				// Associated translation was not deleted
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a resource bundle when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// First, we must create a new resource "testRB" (needed as a foreign key)
				post( "/globalization/bundles?seqID=3", { "bundleCD" : "testRB",
												  		  "bundleName" : "TEST",
					                			  		  "bundleExtranetName" : "GLOBAL-G2G" } );
				
				// Let's create a new ResourceBundle for testing

				var ResourceBundle = getInstance( "ResourceBundle" ).create( prc.data );
				var RBname = ResourceBundle.getResourceBundleName();

				// This record is not contrained and can be deleted
				var event = delete( "/globalization/resourceBundles/#RBname#?seqID=3" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "ResourceBundle" )
				.where( "ResourceBundleName","=", RBname )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Resource bundle deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "ResourceBundleName" : "testRB_ar_AE",
					 "ResourceBundleCD" : "testRB",
					 "ResourceBundleLocaleCD" : "ar_AE",
					 "ResourceBundleTargetLanguageCD" : "AR",
					 "ResourceBundleComment" : "TESTING" };

		structAppend( data, arguments.overrides, true );
		return data;
	}

}
