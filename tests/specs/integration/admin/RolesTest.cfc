component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "roles", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "Role" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new role", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Role" ).count() ).toBe( prc.count, "Roles should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/roles?seqID=2", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "Role" ).count() ).toBe( prc.count+1 , "One more role should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Role created successfully!" );

			} );

			it( "can update a role", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var role = getInstance( "Role" ).create( prc.data );
				var CD = role.getRoleName();

				// one new record should have been added to the database
				expect( getInstance( "Role" ).count() ).toBe( prc.count+1 , "One more role should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/roles/#CD#?seqID=2", validParams({ "roleDescription" : "SUPER TESTING" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedRole = getInstance( "Role" ).findOrFail(CD);
				expect( updatedRole.getRoleDescription()).toBe("SUPER TESTING");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Role updated successfully!" );

			} );

			it( "Create or update require a valid role description", function() {

				expect( getInstance( "Role" ).count() ).toBe( prc.count, "Roles already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/roles?seqID=2", validParams( { "roleDescription" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "role_form_errors" );
				expect( errors.role_form_errors ).toHaveKey( "roleDescription" );

				// No record was added because of the validation failure
				expect( getInstance( "Role" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the RoleName = "APPROVE" exists in the database
				expect( getInstance( "role" ).existsOrFail("APPROVE") ).toBe(true);

				var event = put( "/roles/APPROVE?seqID=2", validParams( { "roleDescription" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "role_form_errors" );
				expect( errors.role_form_errors ).toHaveKey( "roleDescription" );


			} );

			it( "cannot delete a constrained role", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record roleName = "MANAGE" is constrained by a child entity, it cannot be deleted
				var event = delete( "/roles/MANAGE?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "Role" )
				.where( "roleName","=", "MANAGE" )
				.count();

				// Record is still there
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a role when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new record for testing

				var role = getInstance( "Role" ).create( prc.data );
				var CD = role.getRoleName();

				// This record is not contrained and can be deleted
				var event = delete( "/roles/#CD#?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "Role" )
				.where( "roleName","=", CD )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Role deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "roleName" : "TESTER",  
					 "roleDescription" : "TESTING" };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
