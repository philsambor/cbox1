component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "profile scopes", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "ProfileScope" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new profile scope", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "ProfileScope" ).count() ).toBe( prc.count, "Profile scope should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/profileScopes?seqID=2", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "ProfileScope" ).count() ).toBe( prc.count+1 , "One more profile scope should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Profile scope created successfully!" );

			} );

			it( "can update a profile scope", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var profileScope = getInstance( "ProfileScope" ).create( prc.data );
				var ID = profileScope.getProfileScopeID();

				// one new record should have been added to the database
				expect( getInstance( "ProfileScope" ).count() ).toBe( prc.count+1 , "One more profile scope should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/profileScopes/#ID#?seqID=2", validParams({ "profileScopeDescription" : "UPDATED DESCRIPTION" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedProfileScope = getInstance( "ProfileScope" ).findOrFail(ID);
				expect( updatedProfileScope.getProfileScopeName()).toBe("TEST");
				expect( updatedProfileScope.getProfileScopeDescription()).toBe("UPDATED DESCRIPTION");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Profile scope updated successfully!" );

			} );

			it( "Create or update require a unique type and context association", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "ProfileScope" ).count() ).toBe( prc.count, "Profile scopes should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Upon CREATE

				var event = post( "/profileScopes?seqID=2", validParams( { "profileScopeType" : "SERVICE" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Unique index or primary key violation" );

				// No record was added because of the unique index violation
				expect( getInstance( "ProfileScope" ).count() ).toBe( prc.count , "No profile scope should have been added to the database" );

				// Upon UPDATE

				// Make sure the profileScopeID = 1 exists in the database
				expect( getInstance( "ProfileScope" ).existsOrFail(1) ).toBe(true);

				var event = put( "/profileScopes/1?seqID=2", validParams( { "profileScopeType" : "SERVICE" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// A database duplicate index error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Unique index or primary key violation" );

			} );

			it( "Create or update require a valid profile scope name", function() {

				expect( getInstance( "ProfileScope" ).count() ).toBe( prc.count, "Profile scopes already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/profileScopes?seqID=2", validParams( { "profileScopeName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "profileScope_form_errors" );
				expect( errors.profileScope_form_errors ).toHaveKey( "profileScopeName" );

				// No record was added because of the validation failure
				expect( getInstance( "ProfileScope" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the profileScopeID = 1 exists in the database
				expect( getInstance( "ProfileScope" ).existsOrFail(1) ).toBe(true);

				var event = put( "/profileScopes/1?seqID=2", validParams( { "profileScopeName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "profileScope_form_errors" );
				expect( errors.profileScope_form_errors ).toHaveKey( "profileScopeName" );

			} );

			it( "cannot delete a constrained profile scope", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record profileScopeID = 13 is constrained by a child entity, it cannot be deleted
				var event = delete( "/profileScopes/13?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "ProfileScope" )
				.where( "profileScopeID","=", 13 )
				.count();

				// Record is still there
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a profile scope when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new profile scope for testing

				var profileScope = getInstance( "ProfileScope" ).create( prc.data );
				var ID = profileScope.getProfileScopeID();

				// This record ID is not contrained and can be deleted
				var event = delete( "/profileScopes/#ID#?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "ProfileScope" )
				.where( "profileScopeID","=", ID )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Profile scope deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "profileScopeName" : "TEST",
					 "profileScopeType" : "ANY",	
					 "profileScopeContext" : "4PL",	
					 "profileScopeDescription" : "TEST GROUP" };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
