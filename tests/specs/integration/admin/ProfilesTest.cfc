component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "profiles", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "Profile" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new profile", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Profile" ).count() ).toBe( prc.count, "Profiles should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record and verify that the CREATE action is actually called 
				var event = post( "/profiles?seqID=2", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "Profile" ).count() ).toBe( prc.count+1 , "One more profile should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Profile created successfully!" );

			} );

			it( "can update a profile", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Create a new record
				var profile = getInstance( "Profile" ).create( prc.data );
				var ID = profile.getProfileID();

				// one new record should have been added to the database
				expect( getInstance( "Profile" ).count() ).toBe( prc.count+1 , "One more profile should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/profiles/#ID#?seqID=2", validParams({ "ProfileDescription" : "UPDATED PROFILE DESCRIPTION" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedProfile = getInstance( "Profile" ).findOrFail(ID);
				expect( updatedProfile.getProfileName()).toBe("TEST");
				expect( updatedProfile.getProfileDescription()).toBe("UPDATED PROFILE DESCRIPTION");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Profile updated successfully!" );

			} );

			it( "Create or update require a unique process,role and scope association", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Profile" ).count() ).toBe( prc.count, "Profiles should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Upon CREATE

				var event = post( "/profiles?seqID=2", validParams( { "ProfileProcessCD" : "AAA",
															  "ProfileRoleName" : "MANAGE",
															  "ProfileScopeID" : 13	 } ) );
				
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Unique index or primary key violation" );

				// No record was added because of the unique index violation
				expect( getInstance( "Profile" ).count() ).toBe( prc.count , "No profile should have been added to the database" );

				// Upon UPDATE

				// Make sure the ProfileID = 2 exists in the database
				expect( getInstance( "Profile" ).existsOrFail(2) ).toBe(true);

				// The profile composite association ADM/CONTRIBUTE/13 already exists and must be unique
				
				var event = put( "/profiles/2?seqID=2", validParams( { "ProfileProcessCD" : "ADM",
															   		   "ProfileRoleName" : "CONTRIBUTE",
															   		   "ProfileScopeID" : 13 } ) );
				
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// A database duplicate index error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Unique index or primary key violation" );

			} );

			it( "Create or update require a valid profile name", function() {

				expect( getInstance( "Profile" ).count() ).toBe( prc.count, "Profiles already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// upon CREATE

				var event = post( "/profiles?seqID=2", validParams( { "ProfileName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "profile_form_errors" );
				expect( errors.Profile_form_errors ).toHaveKey( "profileName" );

				// No record was added because of the validation failure
				expect( getInstance( "Profile" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the ProfileID = 1 exists in the database
				expect( getInstance( "Profile" ).existsOrFail(1) ).toBe(true);

				var event = put( "/Profiles/1?seqID=2", validParams( { "ProfileName" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "profile_form_errors" );
				expect( errors.Profile_form_errors ).toHaveKey( "profileName" );

			} );

			it( "delete profile CASCADE delete a user profile", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record ProfileID = 1 is constrained by a child entity, it cannot be deleted
				var event = delete( "/profiles/1?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				var isDeleted = getInstance( "Profile" )
				.where( "ProfileID","=", 1 )
				.count();

				// Record is no longer there
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Profile deleted successfully!" );	
	
			} );

			it( "can delete a profile when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Let's create a new profile for testing

				var profile = getInstance( "Profile" ).create( prc.data );
				var ID = Profile.getProfileID();

				// This record ID is not contrained and can be deleted
				var event = delete( "/Profiles/#ID#?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "Profile" )
				.where( "profileID","=", ID )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Profile deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "ProfileName" : "TEST",
					 "ProfileProcessCD" : "PROD",	
					 "ProfileRoleName" : "VIEW",
					 "ProfileScopeID" : 1,	
					 "ProfileDescription" : "TEST PROFILE" };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
