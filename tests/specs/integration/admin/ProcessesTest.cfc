component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "processes", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "Process" ).count(); 
			prc.data = validParams();	

			} );

			it( "can create a new Process", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( getInstance( "Process" ).count() ).toBe( prc.count, "processes should already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// First create a new (un-assigned) dictionary key
				var dictionaryKey = getInstance( "DictionaryKey" ).create( {"DictionaryKeyString" : "TEST",  
					 												 "dictionaryKeyBundleCD" : "menuRB",
																	 "dictionaryKeySourceLanguageCD" : "EN",
																	 "dictionaryKeyGroupID" : 1,
																	 "dictionaryKeySourceTranslation" : "test",
																	 "dictionaryUIKey" : "Test" });
				var str = dictionaryKey.getDictionaryKeyString();

				// Ensure the new dictionary key exists
				expect( getInstance( "DictionaryKey" ).existsOrFail(str) ).toBe(true);

				// Create a new process record and verify that the CREATE action is actually called 
				var event = post( "/processes?seqID=2", prc.data );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");
				
				// Check that one more record was added to the initial record count
				expect( getInstance( "Process" ).count() ).toBe( prc.count+1 , "One more Process should be in the database" );
				
				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Process created successfully!" );

			} );

			it( "can update a Process", function() {

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// First create a new (un-assigned) dictionary key
				var dictionaryKey = getInstance( "DictionaryKey" ).create( {"DictionaryKeyString" : "TEST",  
					 												 "dictionaryKeyBundleCD" : "menuRB",
																	 "dictionaryKeySourceLanguageCD" : "EN",
																	 "dictionaryKeyGroupID" : 1,
																	 "dictionaryKeySourceTranslation" : "test",
																	 "dictionaryUIKey" : "Test" });
				var str = dictionaryKey.getDictionaryKeyString();

				// Ensure the new dictionary key exists
				expect( getInstance( "DictionaryKey" ).existsOrFail(str) ).toBe(true);

				// Create a new record
				var process = getInstance( "Process" ).create( prc.data );
				var CD = Process.getProcessCD();

				// one new record should have been added to the database
				expect( getInstance( "Process" ).count() ).toBe( prc.count+1 , "One more Process should be in the database" );

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/processes/#CD#?seqID=2", validParams({ "ProcessDescription" : "UPDATED DESCRIPTION" }) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedProcess = getInstance( "Process" ).findOrFail(CD);
				expect( updatedProcess.getProcessDescription()).toBe("UPDATED DESCRIPTION");

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Process updated successfully!" );

			} );

			it( "Create or update require a valid Process description", function() {

				expect( getInstance( "Process" ).count() ).toBe( prc.count, "processes already exist in the database" );
				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});

				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// First create a new (un-assigned) dictionary key
				var dictionaryKey = getInstance( "DictionaryKey" ).create( {"DictionaryKeyString" : "TEST",  
					 												 "dictionaryKeyBundleCD" : "menuRB",
																	 "dictionaryKeySourceLanguageCD" : "EN",
																	 "dictionaryKeyGroupID" : 1,
																	 "dictionaryKeySourceTranslation" : "test",
																	 "dictionaryUIKey" : "Test" });
				var str = dictionaryKey.getDictionaryKeyString();

				// Ensure the new dictionary key exists
				expect( getInstance( "DictionaryKey" ).existsOrFail(str) ).toBe(true);
				
				// upon CREATE

				var event = post( "/processes?seqID=2", validParams( { "ProcessDescription" : "" } ) );
				expect( event.getCurrentAction() ).toBe("create", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "Process_form_errors" );
				expect( errors.Process_form_errors ).toHaveKey( "ProcessDescription" );

				// No record was added because of the validation failure
				expect( getInstance( "Process" ).count() ).toBe( prc.count );

				// upon UPDATE

				// Make sure the ProcessCD = "LOGISTICS" exists in the database
				expect( getInstance( "Process" ).existsOrFail("LOG") ).toBe(true);

				var event = put( "/processes/LOG?seqID=2", validParams( { "ProcessDescription" : "" } ) );
				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");

				// I should get a validation error
				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "Process_form_errors" );
				expect( errors.Process_form_errors ).toHaveKey( "ProcessDescription" );


			} );

			it( "cannot delete a constrained Process", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// record ProcessCD = "AAA" is constrained by a child entity, it cannot be deleted
				var event = delete( "/processes/AAA?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// A database referential integrity error should be raised

				var errors = flash.getAll();
				expect( errors ).toBeStruct();
				expect( errors ).toHaveKey( "coldbox_messagebox" );
				expect( errors.coldbox_messagebox ).toHaveKey( "message");
				expect( errors.coldbox_messagebox.message ).toInclude( "Referential integrity constraint violation" );

				var isNotDeleted = getInstance( "Process" )
				.where( "ProcessCD","=", "AAA" )
				.count();

				// Record is still there
				expect(isNotDeleted).toBe(1);	
	
			} );

			it( "can delete a Process when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );

				// First create a new (un-assigned) dictionary key
				var dictionaryKey = getInstance( "DictionaryKey" ).create( {"DictionaryKeyString" : "TEST",  
					 												 "dictionaryKeyBundleCD" : "menuRB",
																	 "dictionaryKeySourceLanguageCD" : "EN",
																	 "dictionaryKeyGroupID" : 1,
																	 "dictionaryKeySourceTranslation" : "test",
																	 "dictionaryUIKey" : "Test" });
				var str = dictionaryKey.getDictionaryKeyString();

				// Ensure the new dictionary key exists
				expect( getInstance( "DictionaryKey" ).existsOrFail(str) ).toBe(true);
				
				// Let's create a new Process for testing

				var Process = getInstance( "Process" ).create( prc.data );
				var CD = Process.getProcessCD();

				// This record is not contrained and can be deleted
				var event = delete( "/processes/#CD#?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// should no longer be found in the database
				var isDeleted = getInstance( "Process" )
				.where( "ProcessCD","=", CD )
				.count();

				// record is no longer in the database
				expect(isDeleted).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "Process deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "ProcessCD" : "TESTING",  
					 "ProcessDictionaryKey" : "TEST",
					 "ProcessDescription" : "TEST DESCRIPTION",
					 "ProcessIcon" : "" };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
