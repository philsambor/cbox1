component extends="tests.resources.BaseIntegrationSpec" {

	property name="flash" inject="provider:coldbox:flash";
	property name="auth" inject="AuthenticationService@cbauth";

	function run(){
		describe( "user profiles", function() {

			beforeEach( function(){

			// Logout the user	
			auth.logout();	

			// Reset parameters
			prc.count = getInstance( "UserProfile" ).count(); 
			prc.data = validParams();

			// In this test suite, H2 Java SQL database grammar (used for testing) does not 
			// properly recognize and implement composite primary keys. 
			// Because of this, inserting two distinct profiles for the same user causes
			// a foreign key constraint violation error.
			// I worked around this issue by registering a new user for every test.
			// This issue only arises in testing, never in the app using the MySQL database.

			} );

			it( "can create a user profile for a newly registered user", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				//expect( getInstance( "UserProfile" ).count() ).toBe( prc.count, "UserProfile should not already exist in the database" );
				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Ensure that no user profile already exists for this test
				expect( getInstance( "UserProfile" ).count() ).toBe(0);

				// Let us register a new user

				var userData =  { "email" : "abc@example.com", 
					 "password" : "one",
					 "emailNotify" : "abc@example.com",
					 "nickname" : "aha", 
					 "firstname" : "Max",
					 "lastname" : "Planck" };

				var newUser = getInstance( "User" ).create( userData );

				expect( getInstance( "User" ).count() ).toBeGTE( 2, "At least two users should exist in the database" );
				var userID = newUser.getId();

				// Create a new user profile and assign it to that new user

				var userProfileData = { "userLoginID" : userID,
					 					"userProfileID" : 1,	
					 					"userWorkgroupID" : 3,	
					 					"userProfileOK" : true };
				
				var newUserProfile = getInstance( "UserProfile" ).create( userProfileData );

				var ID1 = newUserProfile.getUserLoginID();
				var ID2 = newUserProfile.getUserProfileID();

				expect(ID1).toBe(userID);
				expect(ID2).toBe(1);
				
				// Check that one more user profile record was added
				expect( getInstance( "UserProfile" ).count() ).toBe(1);
				
				// Display a success message
				// var feedback = flash.getAll();
				// expect( feedback.coldbox_messagebox.message ).toInclude( "User profile created successfully!" );

			} );

			it( "can update a user profile", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														 });
				// login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should be logged in" );

				// Ensure that we have not yet added a new user profile
				expect( getInstance( "UserProfile" ).count() ).toBe(0);

				// Let us register a new user

				var userData =  { "email" : "johan@example.com", 
					 "password" : "password",
					 "emailNotify" : "johan@example.com",
					 "nickname" : "wow", 
					 "firstname" : "Penelope",
					 "lastname" : "Johansson" };

				var newUser = getInstance( "User" ).create( userData );

				expect( getInstance( "User" ).count() ).toBeGTE( 2, "At least two users should exist in the database" );

				var userID = newUser.getId();

				// Create a new user profile and assign it to that new user

				var userProfileData = { "userLoginID" : userID,
					 					"userProfileID" : 2,	
					 					"userWorkgroupID" : 4,	
					 					"userProfileOK" : true };
				
				var newUserProfile = getInstance( "UserProfile" ).create( userProfileData );

				var ID1 = newUserProfile.getUserLoginID();
				var ID2 = newUserProfile.getUserProfileID();

				expect(ID1).toBe(userID);
				expect(ID2).toBe(2);

				// Now, update that record and verify that the action called is an UPDATE
				var event = put( "/userProfiles/#ID1#/#ID2#/seqID/2", validParams({ "userWorkgroupID" : 2,
			 																	    "userProfileOK" : false }) );

				expect( event.getCurrentAction() ).toBe("update", "I actually hit [#event.getCurrentAction()#]");
				
				// Check data of the updated entity
				var updatedUserProfile = getInstance( "UserProfile" ).findOrFail([ID1,ID2]);
				expect( updatedUserProfile.getUserWorkgroupID()).toBe(2);
				expect( updatedUserProfile.getUserProfileOK()).toBe(false);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "User profile updated successfully!" );

			} );

			it( "can delete a user profile when not constrained", function() {

				expect( auth.check() ).toBeFalse( "The user should be logged out" );

				// Create a test user session
				var user = getInstance( "User" ).create( { "email" : "test@test.com", 
														   "password" : "test"
														});
				// Login the user
				post( "/login", { "email" : "test@test.com", 
								  "password" : "test"
								} );

				expect( auth.check() ).toBeTrue( "The user should now be logged in" );
				
				// Ensure that we have not yet added a new user profile
				expect( getInstance( "UserProfile" ).count() ).toBe(0);

				// Let us register a new user

				var userData =  { "email" : "tiga@example.com", 
					 "password" : "password3",
					 "emailNotify" : "tiga@example.com",
					 "nickname" : "bagus", 
					 "firstname" : "Ibu",
					 "lastname" : "Satu" };
				
				var newUser = getInstance( "User" ).create( userData );

				expect( getInstance( "User" ).count() ).toBeGTE( 2, "At least two users should exist in the database" );

				var userID = newUser.getId();

				// Create a new user profile and assign it to that new user

				var userProfileData = { "userLoginID" : userID,
					 					"userProfileID" : 3,	
					 					"userWorkgroupID" : 5,	
					 					"userProfileOK" : true };
				
				var newUserProfile = getInstance( "UserProfile" ).create( userProfileData );

				var ID1 = newUserProfile.getUserLoginID();
				var ID2 = newUserProfile.getUserProfileID();

				expect(ID1).toBe(userID);
				expect(ID2).toBe(3);

				// Check that one more user profile record was added
				expect( getInstance( "UserProfile" ).count() ).toBe(1);

				// Now let's delete this newly created user profile
				var event = delete( "/userProfiles/#ID1#/#ID2#?seqID=2" );
				expect( event.getCurrentAction() ).toBe("delete", "I actually hit [#event.getCurrentAction()#]");

				// Check that the user profile is gone
				expect( getInstance( "UserProfile" ).count() ).toBe(0);

				// Display a success message
				var feedback = flash.getAll();
				expect( feedback.coldbox_messagebox.message ).toInclude( "User profile deleted successfully!" );
	
			} );

		} );
	}

	private struct function validParams( struct overrides = {} ){
		var data = { "userLoginID" : 7,
					 "userProfileID" : 8,	
					 "userWorkgroupID" : 5,	
					 "userProfileOK" : true };
		structAppend( data, arguments.overrides, true );
		return data;
	}

}
