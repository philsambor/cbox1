/**
* I am the Translations handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";
property name="toAscii" inject="models.utils.TextConversionSVC";  

  function index(event, rc, prc)  {

  var locale = "ar_AE";  
  prc.translations =  getInstance("Translation").approvedTranslationsForLocale(locale);

  event.setView( "translations/index");
  
  }

 function show(event, rc, prc)  { 

  prc.translation = getInstance( "Translation" ).findOrFail(rc.translationID);
  event.setView( "translations/show");

  }

  function new(event, rc, prc) {
  
  param prc.errors = flash.get( "translation_form_errors", {} ); 
  
  // By default, the translation status is set to false
	event.paramValue("translationStatus",0);
  
  // Retrieve all dictionary keys associated with translations and store them to an array

  var myArray = ArrayNew();
  myArray = getInstance( "Translation" ).distinct().values( "translationDictionaryKey" );
  
  var bundleCD = "MenuRB";

  // These data are needed to populate the drop-down select object in the view

  prc.dictionaryKeys = getInstance( "DictionaryKey" ).keysWithoutTranslation(bundleCD, myArray);
  prc.resourceBundles = getInstance( "ResourceBundle" ).with( "language" ).all();

  if (NOT arraylen(prc.dictionaryKeys)) {

		// Return the warning message
		messagebox.setMessage( type = "warn", message = "No dictionary key available for translation - Add at least one dictionary key to create a translation " );

    };       

  event.setView( "translations/new");

  }

  function create(event, rc, prc) {  

  var result = validateModel(
			target = rc,
			constraints = {
        "translationTranslatedKey" : { "required" : true },
        "translationStatus" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "translation_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}

  var tslConverted = toAscii.getConvertedText(rc.translationTranslatedKey);  

  try {

    var translation = getInstance( "Translation" ).create( {
    "translationBundleName" : rc.translationBundleName,
    "translationDictionaryKey" : Ucase(rc.translationDictionaryKey),
    "translationTranslatedKey" : rc.translationTranslatedKey,
    "translationTranslatedAscii" : tslConverted,
    "translationStatus" : rc.translationStatus
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Translation created successfully!" );
  //relocate( "translations" );
  relocate( event = "translations", queryString = "seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.translation = getInstance( "Translation" ).findOrFail(rc.translationID);
  param prc.errors = flash.get( "translation_form_errors", {} ); 

  // Get instance parameter values to manage UI controls on the view
  prc.translationStatus = prc.translation.getTranslationStatus();

  event.setView( "translations/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "translationTranslatedKey" : { "required" : true },
        "translationStatus" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "translation_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var translation = getInstance( "Translation" ).findOrFail(rc.translationID);
  translation.setTranslationBundleName( rc.translationBundleName );
  translation.setTranslationDictionaryKey( Ucase(rc.translationDictionaryKey) );
  translation.setTranslationTranslatedKey( rc.translationTranslatedKey);
  translation.setTranslationTranslatedAscii( toAscii.getConvertedText(rc.translationTranslatedKey) );
  translation.setTranslationStatus( rc.translationStatus);
  translation.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Translation updated successfully!" );
  relocate( event = "globalization.translations.#translation.getTranslationID()#", queryString = "seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var translation = getInstance( "Translation" ).findOrFail(rc.translationID);

  try {

  translation.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Translation deleted successfully!" );
  //relocate( "globalization.translations?seqID=#rc.seqID#" );
  relocate( event = "globalization.translations", queryString="seqID=#rc.seqID#" );

  }


}