/**
* I am the Languages handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  //prc.languages = getInstance( "Language" ).all();

  prc.languages = getInstance( "Language" )
    .orderBy( "languageCD" )
    .get();

  event.setView( "languages/index");    

  }

 function show(event, rc, prc)  {

  prc.language = getInstance( "Language" ).findOrFail(rc.languageCD);
  event.setView( "languages/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "language_form_errors", {} ); 

  event.setView( "languages/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "languageCD" : { "required" : true },
				"languageName" : { "required" : true,
                           "uniqueInDatabase" : { "table" : "tab_languages", "column" : "lang_language_nm" } }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "language_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var language = getInstance( "Language" ).create( {
    "languageCD" : Ucase(rc.languageCD),
    "languageName" : Ucase(rc.languageName)
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Language created successfully!" );
  relocate( "globalization.languages?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.language = getInstance( "Language" ).findOrFail(rc.languageCD);
  param prc.errors = flash.get( "language_form_errors", {} ); 

  event.setView( "languages/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        //"languageCD" : { "required" : true },
				"languageName" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "language_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var language = getInstance( "Language" ).findOrFail(rc.languageCD);
  language.setLanguageName( Ucase(rc.languageName) );
  language.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Language updated successfully!" );
  relocate("globalization.languages.#language.getLanguageCD()#?seqID=#rc.seqID#");

  }

  function delete(event, rc, prc) {
  var language = getInstance( "Language" ).findOrFail(rc.languageCD);

  try {

  language.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Language deleted successfully!" );
  relocate( "globalization.languages?seqID=#rc.seqID#" );

  }

}