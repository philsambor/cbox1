/**
* I am the resourceBundles handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  prc.resourceBundles = getInstance( "ResourceBundle" )
    .with( "language" )
    .orderBy( "resourceBundleLocaleCD" )
    .get();

  event.setView( "resourceBundles/index");    

  }

 function show(event, rc, prc)  {

  prc.resourceBundle = getInstance( "ResourceBundle" ).findOrFail(rc.resourceBundleName);
  event.setView( "resourceBundles/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "ResourceBundle_form_errors", {} ); 

  // Get data for the view's drop-down select controls
  prc.bundles = getInstance( "Bundle" ).all();
  prc.locales = getInstance( "Locale" ).all();
  prc.languages = getInstance( "Language" ).all();

  event.setView( "resourceBundles/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
				"ResourceBundleComment" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "ResourceBundle_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var ResourceBundle = getInstance( "ResourceBundle" ).create( {
    "REsourceBundleName" : "#rc.resourceBundleCD#_#rc.resourceBundleLocaleCD#",
    "ResourceBundleCD" : rc.resourceBundleCD,
    "ResourceBundleLocaleCD" : rc.resourceBundleLocaleCD,
    "ResourceBundleTargetLanguageCD" : Ucase(rc.resourceBundleTargetLanguageCD),
    "ResourceBundleComment" : Ucase(rc.resourceBundleComment)
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Resource bundle created successfully!" );
  relocate( "globalization.resourceBundles?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.resourceBundle = getInstance( "ResourceBundle" ).findOrFail(rc.resourceBundleName);
  param prc.errors = flash.get( "ResourceBundle_form_errors", {} ); 

  event.setView( "resourceBundles/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "ResourceBundleComment" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "ResourceBundle_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var resourceBundle = getInstance( "ResourceBundle" ).findOrFail(rc.resourceBundleName);
  resourceBundle.setResourceBundleCD( rc.resourceBundleCD);
  resourceBundle.setResourceBundleLocaleCD( rc.resourceBundleLocaleCD);
  resourceBundle.setResourceBundleTargetLanguageCD( Ucase(rc.ResourceBundleTargetLanguageCD) );
  resourceBundle.setResourceBundleComment( Ucase(rc.ResourceBundleComment) );
  resourceBundle.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Resource bundle updated successfully!" );
  relocate( "globalization.resourceBundles.#resourceBundle.getResourceBundleName()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var resourceBundle = getInstance( "ResourceBundle" ).findOrFail(rc.resourceBundleName);

  try {

  resourceBundle.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Resource bundle deleted successfully!" );
  relocate( "globalization.resourceBundles?seqID=#rc.seqID#" );

  }


}