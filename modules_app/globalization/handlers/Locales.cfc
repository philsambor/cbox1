/**
* I am the Locales handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";
property name="tenantCompany" inject="models/TenantCompanySVC";  

function index(event, rc, prc)  {

  prc.locales = getInstance( "Locale" ).all();

  event.setView( "locales/index");

  }

 function show(event, rc, prc)  {

  prc.locale = getInstance( "Locale" ).findOrFail(rc.localeCD);
  event.setView( "locales/show");

  }

  function new(event, rc, prc)  {
  param prc.errors = flash.get( "locale_form_errors", {} ); 
  
  // I need to retrieve the default locale/language from the TenantCompany instance

	var tenantLocale = tenantCompany.getTenantDefaultLocaleCD();
  prc.defaultLocale = getInstance( "Locale" ).findorFail(tenantLocale);
  prc.defaultLanguageCode = prc.defaultLocale.getLocaleLanguageCD(); 
  prc.defaultLanguageName = prc.defaultLocale.getLanguage().getLanguageName();

  // By default, the new locale is set to use the Gregorian calendar
	event.paramValue("isGregorianCalendar",1);
 
	// These data are needed to populate the drop-down select object in the view
  // Retrieve all languages except the default language used as a filter

	var filter = prc.defaultLanguageCode;
  
  prc.filterLanguages = getInstance( "Language" )
    .where( "languageCD","<>", filter )
    .orderByAsc( "languageCD" )
    .get();


  event.setView( "locales/new");

  }

  function create(event, rc, prc) secured {

  var result = validateModel(
			target = rc,
			constraints = {
        "localeCD" : { "required" : true },
        "localeLanguageCD" : { "required" : true },
				"localeName" : { "required" : true },
        "calendarLang" : { "required" : true },
        "isGregorianCalendar" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "locale_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var locale = getInstance( "Locale" ).create( {
    "localeCD" : rc.localeCD,
    "localeLanguageCD" : rc.localeLanguageCD,
    "localeName" : Ucase(rc.localeName),
    "calendarLang" : Lcase(rc.calendarLang),
    "isGregorianCalendar" : rc.isGregorianCalendar
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Locale created successfully!" );
  relocate( "globalization.locales?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.locale = getInstance( "Locale" ).findOrFail(rc.localeCD);
  param prc.errors = flash.get( "locale_form_errors", {} ); 

  // Get instance parameter values to manage UI controls on the view
  prc.isGregorianCalendar = prc.locale.getIsGregorianCalendar();

  // These data are needed to populate the drop-down select object in the view
  // Retrieve all languages except the default language used as a filter

  prc.filter = prc.locale.getLocaleLanguageCD();
  prc.filterName = prc.locale.getLanguage().getLanguageName();
  
  prc.filterLanguages = getInstance( "Language" )
    .where( "languageCD","<>", prc.filter )
    .orderByAsc( "languageCD" )
    .get();

  event.setView( "locales/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "localeLanguageCD" : { "required" : true },
				"localeName" : { "required" : true },
        "calendarLang" : { "required" : true },
        "isGregorianCalendar" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "locale_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var locale = getInstance( "Locale" ).findOrFail(rc.localeCD);
  locale.setLocaleLanguageCD( rc.localeLanguageCD );
  locale.setLocaleName( Ucase(rc.localeName) );
  locale.setCalendarLang( Lcase(rc.calendarLang) );
  locale.setIsGregorianCalendar( rc.isGregorianCalendar );
  locale.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Locale updated successfully!" );
  relocate( "globalization.locales.#locale.getLocaleCD()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var locale = getInstance( "Locale" ).findOrFail(rc.localeCD);

  try {

  locale.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Locale deleted successfully!" );
  relocate( "globalization.locales?seqID=#rc.seqID#" );

  }


}