/**
* I am the ressources handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  prc.bundles = getInstance( "Bundle" ).all();

  // prc.bundles = getInstance( "Bundle" )
  //   .orderBy( "bundleCD" )
  //   .get();

  event.setView( "bundles/index");  

  }

 function show(event, rc, prc)  {

  prc.bundle = getInstance( "Bundle" ).findOrFail(rc.bundleCD);
  event.setView( "bundles/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "bundle_form_errors", {} ); 

  event.setView( "bundles/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "bundleCD" : { "required" : true },
        "bundleName" : { "required" : true },
				"bundleExtranetName" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "bundle_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var bundle = getInstance( "Bundle" ).create( {
    "bundleCD" : rc.bundleCD,
    "bundleName" : Ucase(rc.bundleName),
    "bundleExtranetName" : Ucase(rc.bundleExtranetName)
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Bundle created successfully!" );
  relocate( "globalization.bundles?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.bundle = getInstance( "Bundle" ).findOrFail(rc.bundleCD);
  param prc.errors = flash.get( "bundle_form_errors", {} ); 

  event.setView( "bundles/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "bundleName" : { "required" : true },
				"bundleExtranetName" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "bundle_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var bundle = getInstance( "Bundle" ).findOrFail(rc.bundleCD);
  bundle.setBundleName( Ucase(rc.bundleName) );
  bundle.setBundleExtranetName( Ucase(rc.bundleExtranetName) );
  bundle.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Bundle updated successfully!" );
  relocate( "globalization.bundles.#bundle.getBundleCD()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var bundle = getInstance( "Bundle" ).findOrFail(rc.bundleCD);

  try {

  bundle.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Bundle deleted successfully!" );
  relocate( "globalization.bundles?seqID=#rc.seqID#" );

  }


}