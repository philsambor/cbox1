/**
* I am the DictionaryGroups handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  prc.dictionaryGroups = getInstance( "DictionaryGroup" ).all();

  event.setView( "dictionaryGroups/index");

  }

 function show(event, rc, prc)  {

  prc.dictionaryGroup = getInstance( "DictionaryGroup" ).findOrFail(rc.dictionaryGroupID);
  event.setView( "dictionaryGroups/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "dictionaryGroup_form_errors", {} ); 

  event.setView( "dictionaryGroups/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
	      "dictionaryGroupName" : { "required" : true,
                                  "uniqueInDatabase" : { "table" : "tab_dictionary_groups", "column" : "dgr_group_nm" } },
	      "dictionaryGroupDescription" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "dictionaryGroup_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var dictionaryGroup = getInstance( "DictionaryGroup" ).create( {
    "dictionaryGroupName" : Ucase(rc.dictionaryGroupName),
    "dictionaryGroupDescription" : Ucase(rc.dictionaryGroupDescription)
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Dictionary group created successfully!" );
  relocate( "globalization.dictionaryGroups?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.dictionaryGroup = getInstance( "DictionaryGroup" ).findOrFail(rc.dictionaryGroupID);
  param prc.errors = flash.get( "dictionaryGroup_form_errors", {} ); 

  event.setView( "dictionaryGroups/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "dictionaryGroupName" : { "required" : true },
	      "dictionaryGroupDescription" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "dictionaryGroup_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var dictionaryGroup = getInstance( "DictionaryGroup" ).findOrFail(rc.dictionaryGroupID);
  dictionaryGroup.setDictionaryGroupName( Ucase(rc.dictionaryGroupName) );
  dictionaryGroup.setDictionaryGroupDescription( Ucase(rc.dictionaryGroupDescription) );
  dictionaryGroup.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Dictionary group updated successfully!" );
  relocate( "globalization.dictionaryGroups.#dictionaryGroup.getDictionaryGroupID()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var dictionaryGroup = getInstance( "DictionaryGroup" ).findOrFail(rc.dictionaryGroupID);

  try {

  dictionaryGroup.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Dictionary group deleted successfully!" );
  relocate( "globalization.dictionaryGroups?seqID=#rc.seqID#" );

  }


}