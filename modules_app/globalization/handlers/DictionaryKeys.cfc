/**
* I am the dictionaryKeys handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";

  function index(event, rc, prc)  {

  prc.dictionaryKeys = getInstance( "dictionaryKey" )
    .with( "dictionaryGroup" )
    .orderBy( "dictionaryKeyString" )
    .get();

  event.setView( "dictionaryKeys/index");    

  }

 function show(event, rc, prc)  {
 
  prc.dictionaryKey = getInstance( "DictionaryKey" ).findOrFail(rc.dictionaryKeyString);
  event.setView( "dictionaryKeys/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "dictionaryKey_form_errors", {} ); 

  // Get data for the view's drop-down select controls
  prc.bundles = getInstance( "Bundle" ).all();
  prc.dictionaryGroups = getInstance( "DictionaryGroup" ).all();

  event.setView( "dictionaryKeys/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "dictionaryKeyString" : { "required" : true },
        "dictionaryKeySourceTranslation" : { "required" : true },
        "dictionaryUIKey" : { "required" : true,
                              "uniqueInDatabase" : { "table" : "tab_dictionary_keys", "column" : "dix_translation_key" } }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "dictionaryKey_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var dictionaryKey = getInstance( "dictionaryKey" ).create( {
    "dictionaryKeyString" : Ucase(rc.dictionaryKeyString),
    "dictionaryKeyBundleCD" : rc.dictionaryKeyBundleCD,
    "dictionaryKeySourceLanguageCD" : Ucase(rc.dictionaryKeySourceLanguageCD),
    "dictionaryKeyGroupID" : rc.dictionaryKeyGroupID,
    "dictionaryKeySourceTranslation" : Lcase(rc.dictionaryKeySourceTranslation),
    "dictionaryUIKey" : rc.dictionaryUIKey

  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Dictionary key created successfully!" );
  relocate( "globalization.dictionaryKeys?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.dictionaryKey = getInstance( "DictionaryKey" ).findOrFail(rc.dictionaryKeyString);
  param prc.errors = flash.get( "dictionaryKey_form_errors", {} );

  // These data are needed to populate the drop-down select object in the view
  
  prc.groupfilter = prc.dictionaryKey.getDictionaryKeyGroupID();
  prc.groupfilterName = prc.dictionaryKey.getDictionaryGroup().getDictionaryGroupName();
  prc.bundlefilter = prc.dictionaryKey.getDictionaryKeyBundleCD();
  prc.bundlefilterName = prc.dictionaryKey.getBundle().getBundleName();

  // Retrieve all data for the select drop-down except the one used as a filter
  
  prc.filterDictionaryGroups = getInstance( "DictionaryGroup" )
    .where( "dictionaryGroupID","<>", prc.groupfilter )
    .get();

  prc.filterBundles = getInstance( "Bundle" )
    .where( "bundleCD","<>", prc.bundlefilter )
    .get(); 
  
  event.setView( "dictionaryKeys/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "dictionaryKeySourceTranslation" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "dictionaryKey_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var dictionaryKey = getInstance( "DictionaryKey" ).findOrFail(rc.dictionaryKeyString);
  dictionaryKey.setDictionaryKeyBundleCD( rc.dictionaryKeyBundleCD);
  dictionaryKey.setDictionaryKeySourceLanguageCD( Ucase(rc.dictionaryKeySourceLanguageCD) );
  dictionaryKey.setDictionaryKeyGroupID( rc.dictionaryKeyGroupID );
  dictionaryKey.setDictionaryKeySourceTranslation( LCase(rc.dictionaryKeySourceTranslation) );
  dictionaryKey.setDictionaryUIKey( rc.dictionaryUIKey );
  dictionaryKey.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Dictionary key updated successfully!" );
  relocate( "globalization.dictionaryKeys.#dictionaryKey.getDictionaryKeyString()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var dictionaryKey = getInstance( "DictionaryKey" ).findOrFail(rc.dictionaryKeyString);

  try {

  dictionaryKey.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "dictionary key deleted successfully!" );
  relocate( "globalization.dictionaryKeys?seqID=#rc.seqID#" );

  }

}