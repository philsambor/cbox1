/**
 * The Globalization module home handler
 */
 
component{

	property name="menu" inject="models.MenuContentSVC";

	/**
	 * Home page
	 */

	function index( event, rc, prc ){
		prc.structNav = menu.getAuthNavigation();
		event.setView( "home/index" );
	}

}
