<!--- Views/languages/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "#prc.tsl.screen.LANGUAGE_DETAILS#" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>#prc.tsl.screen.LANGUAGE_CD#</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.language.getLanguageCD()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>#prc.tsl.screen.LANGUAGE_NAME#</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.language.getLanguageName()#</div>
        </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("globalization.languages?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">#prc.tsl.menu.CANCEL#</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>