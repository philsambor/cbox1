<!--- Views/languages/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "#prc.tsl.screen.LANGUAGES_LIST#" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('globalization.languages.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">#prc.tsl.menu.NEW#</a>
        </p>
    </div>    
</div>       
           
<cfif prc.languages.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available languages</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th class="text-center">#prc.tsl.screen.LANGUAGE_CD#</th>
                <th scope="col">#prc.tsl.screen.LANGUAGE_NAME#</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.languages#" index="language">
            <tr>
                <td class="text-center"><a href="#event.buildlink("globalization.languages.#language.getLanguageCD()#.seqID.#rc.seqID#")#" class="card-link">#prc.tsl.menu.VIEW#</a></td>
                <td class="text-center">#language.getLanguageCD()#</td>
                <td >#language.getLanguageName()#</td>
                <td class="text-center"><a href="#event.buildlink("globalization.languages.#language.getLanguageCD()#.edit.seqID.#rc.seqID#")#" class="card-link">#prc.tsl.menu.EDIT#</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>