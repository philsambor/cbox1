<!--- Views/languages/edit.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "#prc.tsl.screen.UPDATE_A_LANGUAGE_RECORD#" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">

        <p class="m-b btn-groups my-2">
        #html.startForm( method = "DELETE", action = event.buildlink( "globalization.languages.#prc.language.getLanguageCD()#.seqID.#rc.seqID#") )#
        <button type="submit" name="btn_delete" class="btn btn-outline-danger">#prc.tsl.menu.DELETE#</button>
        #html.endForm()#
        </p>

        #renderView( "languages/_formLanguage", {
            "method" : "PUT",
            "action" : event.buildlink( "globalization.languages.#prc.language.getLanguageCD()#.seqID.#rc.seqID#" )
        } )#

    </div>    
</div>



</cfoutput>