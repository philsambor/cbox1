<!--- Views/languages/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "#prc.tsl.screen.ADD_A_LANGUAGE_RECORD#" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "languages/_formLanguage", {
    "method" : "POST",
    "action" : event.buildlink( "globalization.languages?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>