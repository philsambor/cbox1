<!--- Views/bundles/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Create a new bundle" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "bundles/_formBundle", {
    "method" : "POST",
    "action" : event.buildlink( "globalization.bundles?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>