<!--- Views/bundles/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of bundles" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('globalization.bundles.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.bundles.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available bundles</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Bundle code</th>
                <th scope="col">Bundle name</th>
                <th scope="col">Bundle extranet name</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.bundles#" index="bundle">
            <tr>
                <td class="text-center"><a href="#event.buildlink("globalization.bundles.#bundle.getBundleCD()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#bundle.getBundleCD()#</td>
                <td >#bundle.getBundleName()#</td>
                <td >#bundle.getBundleExtranetName()#</td>
                <td class="text-center"><a href="#event.buildlink("globalization.bundles.#bundle.getBundleCD()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>