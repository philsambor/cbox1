<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <!---<input type="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle code</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text"  
               class="form-control" 
               name="bundleCD" 
               value="#prc.bundle.getBundleCD()#"
               id="bundleCD" 
               readonly >
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle name</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="bundleName" 
                   value="#prc.bundle.getBundleName()#" 
                   id="bundleName" >
            <cfif prc.errors.keyExists( "BundleName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.bundleName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle extranet name</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="bundleExtranetName" 
                   value="#prc.bundle.getBundleExtranetName()#" 
                   id="bundleExtranetName" >
            <cfif prc.errors.keyExists( "BundleExtranetName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.bundleExtranetName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>       

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle code</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" placeholder="testRB" name="bundleCD" id="bundleCD" >
            <cfif prc.errors.keyExists( "BundleCD" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.bundleCD#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle name</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="bundleName" id="bundleName" >
            <cfif prc.errors.keyExists( "BundleName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.bundleName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div> 

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle extranet name</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="bundleExtranetName" id="ressourceExtranetName" >
            <cfif prc.errors.keyExists( "BundleExtranetName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.bundleExtranetName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>     
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "globalization.bundles?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>