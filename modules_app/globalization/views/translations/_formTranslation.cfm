<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <!---<input type="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Dictionary key</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="translationDictionaryKey" 
                   value="#prc.translation.getTranslationDictionaryKey()#" 
                   id="translationDictionaryKey"
                   readonly >
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Resource bundle</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="translationBundleName" 
                   value="#prc.translation.getTranslationBundleName()#" 
                   id="translationBundleName"
                   readonly >
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Translated key</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="translationTranslatedKey" 
                   value="#prc.translation.getTranslationTranslatedKey()#" 
                   id="translationTranslatedKey" >
            <cfif prc.errors.keyExists( "translationTranslatedKey" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.translationTranslatedKey#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>       
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Translated ASCII</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="translationTranslatedAscii" 
                   value="#prc.translation.getTranslationTranslatedAscii()#" 
                   id="translationTranslatedAscii"
                   readonly >
        </div>

    </div>

    <div class="form-group row">
        
        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Translation status</strong></div>

            <!--- Based on the value passed by the handler, test the current radio button status --->	
                  
            <cfset varTest = prc.translationStatus />

            <cfif varTest>
                <cfset checkStatus1 = "checked" />
                <cfset checkStatus2 = "" />   	
            <cfelse>
                <cfset checkStatus1 = "" /> 
                <cfset checkStatus2 = "checked" />   
            </cfif>

            <!--- check or uncheck the radio switch accordingly --->

            <div class="col-sm-4 col-md-9 my-2">

                <label class="form-check form-check-inline">
                    <input type="radio" value="1" name="translationStatus" #checkStatus1#>
                    <i class="theme"></i>
                    &nbsp;Translated
                </label>

                <label class="form-check form-check-inline">
                    <input type="radio" value="0" name="translationStatus" #checkStatus2#>
                    <i class="theme no-icon"></i>
                    &nbsp;Not translated
                </label>

            </div>      
    </div>                     

    <cfelse>

    <!--- in creation mode --->

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Dictionary key</strong></div>

        <div class="col-sm-4 col-md-9">
        <select id="translationDictionaryKey" class="form-control custom-select" name="translationDictionaryKey" >
            <cfloop array= #prc.dictionaryKeys# index="dictionaryKey">	
            <option value="#dictionaryKey.getDictionaryKeyString()#">#dictionaryKey.getDictionaryKeyString()#&nbsp;|&nbsp;(#dictionaryKey.getBundle().getBundleCD()#)</option>
            </cfloop>
       	</select>     
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Resource bundle</strong></div>

        <div class="col-sm-4 col-md-9">
          <select id="translationBundleName" class="form-control custom-select" name="translationBundleName" >
            <cfloop array= #prc.resourceBundles# index="resourceBundle">	
            <option value="#resourceBundle.getResourceBundleCD()#">#resourceBundle.getResourceBundleCD()#&nbsp;|&nbsp;#resourceBundle.getResourceBundleComment()#</option>
            </cfloop>
       	  </select>      
        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Translated key</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="translationTranslatedKey" id="translationTranslatedKey" >
            <cfif prc.errors.keyExists( "translationTranslatedKey" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.translationTranslatedKey#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">
        
        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Translation status</strong></div>

            <!--- when this boolean value changes, toggle the radio button visual ---> 
            
            <cfif event.getValue("translationStatus") >
                <cfset checkStatus1 = "checked" />
                <cfset checkStatus2 = "" />   	
                <cfelse>
                <cfset checkStatus1 = "" /> 
                <cfset checkStatus2 = "checked" />   
            </cfif>

            <!--- check or uncheck the radio switch accordingly --->

               <div class="col-sm-4 col-md-9 my-2">

                  <label class="form-check form-check-inline">
                     <input type="radio" value="1" name="translationStatus" #checkStatus1#>
                     <i class="theme"></i>
                     &nbsp;Translated
                  </label>

                  <label class="form-check form-check-inline">
                     <input type="radio" value="0" name="translationStatus" #checkStatus2#>
                     <i class="theme no-icon"></i>
                     &nbsp;Not translated
                  </label>

            </div>      
        </div>            
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "globalization.translations?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>