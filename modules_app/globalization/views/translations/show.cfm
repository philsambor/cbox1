<!--- Views/translations/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "Translation details" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Translation ID</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.translation.getTranslationID()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Resource bundle</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.translation.getTranslationBundleName()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Dictionary key</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.translation.getTranslationDictionaryKey()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Translated key</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.translation.getTranslationTranslatedKey()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Translated ASCII</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.translation.getTranslationTranslatedAscii()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

    <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Translation status</strong></div>
    <div class="col-sm-4 col-md-9">

        <cfif NOT prc.translation.getTranslationStatus() >          		
            <cfset status = "Not translated" />
            <div class="form-control" readonly><span class="badge badge-warning">#status#</span></div>  		
        <cfelse>	
            <cfset status = "Translated" />
            <div class="form-control" readonly><span class="badge badge-success">#status#</span></div> 
        </cfif>

    </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("globalization.translations?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">Back</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>