<!--- Views/translations/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of translations" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('globalization.translations.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.translations.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available translations</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Resource bundle</th>
                <th scope="col">Dictionary key</th>
                <th scope="col">Translated key</th>
                <th scope="col">Translated ASCII</th>
                <th class="text-center">Translation status</th>
                <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.translations#" index="translation">
            <tr>
                <td class="text-center"><a href="#event.buildlink("globalization.translations.#translation.getTranslationID()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#translation.getTranslationBundleName()#</td>
                <td >#translation.getTranslationDictionaryKey()#</td>
                <td >#translation.getTranslationTranslatedKey()#</td>
                <td >#translation.getTranslationTranslatedAscii()#</td>
                <cfif NOT translation.getTranslationStatus() >          		
                    <cfset status = "Not translated" />
                    <td class="text-center"><span class="badge badge-warning">#status#</span></td>  		
                    <cfelse>	
                    <cfset status = "Translated" />
                    <td class="text-center"><span class="badge badge-success">#status#</span></td> 
                </cfif> 
                <td class="text-center"><a href="#event.buildlink("globalization.translations.#translation.getTranslationID()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>