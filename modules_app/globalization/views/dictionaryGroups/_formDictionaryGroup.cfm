<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <!---<input type="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Dictionary group name</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="DictionaryGroupName" 
                   value="#prc.dictionaryGroup.getDictionaryGroupName()#" 
                   id="dictionaryGroupName" >
            <cfif prc.errors.keyExists( "dictionaryGroupName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryGroupName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Dictionary group description</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="DictionaryGroupDescription" 
                   value="#prc.dictionaryGroup.getDictionaryGroupDescription()#" 
                   id="dictionaryGroupName" >
            <cfif prc.errors.keyExists( "dictionaryGroupDescription" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryGroupDescription#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>               

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Dictionary group name</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="DictionaryGroupName" id="dictionaryGroupName" >
            <cfif prc.errors.keyExists( "dictionaryGroupName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryGroupName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Dictionary group description</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="dictionaryGroupDescription" id="dictionaryGroupDescription" >
            <cfif prc.errors.keyExists( "dictionaryGroupDescription" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryGroupDescription#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>    
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "globalization.dictionaryGroups?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>