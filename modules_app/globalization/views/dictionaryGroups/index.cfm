<!--- Views/dictionaryGroups/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of dictionary groups" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('globalization.dictionaryGroups.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.dictionaryGroups.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available dictionary groups</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Dictionary group name</th>
                <th scope="col">Dictionary group description</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.dictionaryGroups#" index="dictionaryGroup">
            <tr>
                <td class="text-center"><a href="#event.buildlink("globalization.dictionaryGroups.#dictionaryGroup.getDictionaryGroupID()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#dictionaryGroup.getDictionaryGroupName()#</td>
                <td >#dictionaryGroup.getDictionaryGroupDescription()#</td>
                <td class="text-center"><a href="#event.buildlink("globalization.dictionaryGroups.#dictionaryGroup.getDictionaryGroupID()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>