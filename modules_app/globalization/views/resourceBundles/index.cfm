<!--- Views/resourceBundles/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of resource bundles" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('globalization.resourceBundles.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.resourceBundles.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available resource bundles</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Resource bundle</th>
                <th scope="col">Bundle</th>
                <th scope="col">Locale</th>
                <th scope="col">Target language</th>
                <th scope="col">Resource bundle comment</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.resourceBundles#" index="resourceBundle">
            <tr>
                <td class="text-center"><a href="#event.buildlink("globalization.resourceBundles.#resourceBundle.getResourceBundleName()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#resourceBundle.getResourceBundleName()#</td>
                <td >#resourceBundle.getResourceBundleCD()#</td>
                <td >#resourceBundle.getResourceBundleLocaleCD()#</td>
                <td >#resourceBundle.getResourceBundleTargetLanguageCD()#&nbsp;|&nbsp;#resourceBundle.getLanguage().getLanguageName()#</td>
                <td >#resourceBundle.getResourceBundleComment()#</td>
                <td class="text-center"><a href="#event.buildlink("globalization.resourceBundles.#resourceBundle.getResourceBundleName()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>