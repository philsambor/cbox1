<!--- Views/resourceBundles/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Create a new resource bundle" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "resourceBundles/_formResourceBundle", {
    "method" : "POST",
    "action" : event.buildlink( "globalization.resourceBundles?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>