<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <!---<input type="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Resource bundle name</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text"  
               class="form-control" 
               name="resourceBundleName" 
               value="#prc.resourceBundle.getResourceBundleName()#"
               id="resourceBundleCD" 
               readonly >
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Target language code</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="resourceBundleTargetLanguageCD" 
                   value="#prc.resourceBundle.getResourceBundleTargetLanguageCD()#" 
                   id="resourceBundleTargetLanguageCD" 
                   readonly>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle code</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="resourceBundleCD" 
                   value="#prc.resourceBundle.getResourceBundleCD()#" 
                   id="resourceBundleCD" 
                   readonly>
            
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Locale code</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="resourceBundleLocaleCD" 
                   value="#prc.resourceBundle.getResourceBundleLocaleCD()#" 
                   id="resourceBundleLocaleCD" 
                   readonly>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Resource bundle comment</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="resourceBundleComment" 
                   value="#prc.resourceBundle.getResourceBundleComment()#" 
                   id="resourceBundleComment" >
            <cfif prc.errors.keyExists( "resourceBundleComment" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.resourceBundleComment#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>            

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Target language code</strong></div>

        <div class="col-sm-4 col-md-9">
            <select id="resourceBundleTargetLanguageCD" class="form-control custom-select" name="resourceBundleTargetLanguageCD" >
                <cfloop array= #prc.languages# index="language">	
                <option value="#language.getLanguageCD()#">#language.getLanguageCD()#&nbsp;|&nbsp;#language.getLanguageName()#</option>
                </cfloop>
            </select> 
        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Bundle code</strong></div>

        <div class="col-sm-4 col-md-9">
            <select id="resourceBundleCD" class="form-control custom-select" name="resourceBundleCD" >
                <cfloop array= #prc.bundles# index="bundle">	
                <option value="#bundle.getBundleCD()#">#bundle.getBundleCD()#&nbsp;|&nbsp;#bundle.getBundleName()#</option>
                </cfloop>
            </select>
        </div>

    </div> 

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Locale code</strong></div>

        <div class="col-sm-4 col-md-9">
            <select id="resourceBundleLocaleCD" class="form-control custom-select" name="resourceBundleLocaleCD" >
                <cfloop array= #prc.locales# index="locale">	
                <option value="#locale.getLocaleCD()#">#locale.getLocaleCD()#&nbsp;|&nbsp;#locale.getLocaleName()#</option>
                </cfloop>
            </select>
        </div>

    </div> 

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Resource bundle comment</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="resourceBundleComment" id="resourceBundleComment" >
            <cfif prc.errors.keyExists( "resourceBundleComment" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.resourceBundleComment#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>               
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "globalization.resourceBundles?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>