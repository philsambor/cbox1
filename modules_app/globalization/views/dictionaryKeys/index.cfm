<!--- Views/dictionaryKeys/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of dictionary keys" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('globalization.dictionaryKeys.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.dictionaryKeys.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available dictionary keys</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Dictionary key</th>
                <th scope="col">Bundle</th>
                <th scope="col">Dictionary group</th>
                <!---<th scope="col">Source translation</th>--->
                <!---<th scope="col">Dictionary UI key</th>--->
                <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.dictionaryKeys#" index="dictionaryKey">
            <tr>
                <td class="text-center"><a href="#event.buildlink("globalization.dictionaryKeys.#dictionaryKey.getdictionaryKeyString()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#dictionaryKey.getDictionaryKeyString()#</td>
                <td >#dictionaryKey.getDictionaryKeyBundleCD()#</td>
                <td >#dictionaryKey.getDictionaryGroup().getDictionaryGroupName()#</td>
                <!---<td >#dictionaryKey.getDictionaryKeySourceLanguageCD()#&nbsp;|&nbsp;#dictionaryKey.getDictionaryKeySourceTranslation()#</td>--->
                <!---<td >#dictionaryKey.getDictionaryUIKey()#</td>--->
                <td class="text-center"><a href="#event.buildlink("globalization.dictionaryKeys.#dictionaryKey.getdictionaryKeyString()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>