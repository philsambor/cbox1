<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <input type="hidden" name="_token" value="#csrfGenerateToken()#" />
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary key string</strong></div>

        <div class="col-sm-4 col-md-8">
        <input type="text"  
               class="form-control" 
               name="dictionaryKeyString" 
               value="#prc.dictionaryKey.getdictionaryKeyString()#"
               id="dictionaryKeyString" 
               readonly >
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary key bundle</strong></div>

        <div class="col-sm-4 col-md-8">
            <select id="dictionaryKeyBundleCD" class="form-control custom-select" name="dictionaryKeyBundleCD" >
                <option value="#prc.bundlefilter#" selected="selected">#prc.bundlefilter#&nbsp;|&nbsp;#prc.bundlefilterName#</option>
                <cfloop array= #prc.filterBundles# index="bundle">	
                <option value="#bundle.getBundleCD()#">#bundle.getBundleCD()#&nbsp;|&nbsp;#bundle.getBundleName()#</option>
                </cfloop>
            </select>
            
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary group</strong></div>

        <div class="col-sm-4 col-md-8">
            <select id="dictionaryKeyGroupID" class="form-control custom-select" name="dictionaryKeyGroupID" >
                <option value="#prc.groupfilter#" selected="selected">#prc.groupfilter#&nbsp;|&nbsp;#prc.groupfilterName#</option>
                <cfloop array= #prc.filterDictionaryGroups# index="dictionaryGroup">	
                <option value="#dictionaryGroup.getDictionaryGroupID()#">#dictionaryGroup.getDictionaryGroupID()#&nbsp;|&nbsp;#dictionaryGroup.getDictionaryGroupName()#</option>
                </cfloop>
            </select>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Source language</strong></div>

        <div class="col-sm-4 col-md-8">    
            <input type="text" 
                   class="form-control" 
                   name="dictionaryKeySourceLanguageCD" 
                   value="#prc.dictionaryKey.getDictionaryKeySourceLanguageCD()#" 
                   id="dictionaryKeySourceLanguageCD"
                   readonly > 
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary key source translation</strong></div>
        <div class="col-sm-4 col-md-8">    
            <input type="text" 
                   class="form-control" 
                   name="dictionaryKeySourceTranslation" 
                   value="#prc.dictionaryKey.getDictionaryKeySourceTranslation()#" 
                   id="dictionaryKeySourceTranslation" >
            <cfif prc.errors.keyExists( "dictionaryKeySourceTranslation" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryKeySourceTranslation#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary UI key</strong></div>
        <div class="col-sm-4 col-md-8">    
            <input type="text" 
                   class="form-control" 
                   name="dictionaryUIKey" 
                   value="#prc.dictionaryKey.getDictionaryUIKey()#" 
                   id="dictionaryUIKey"
                   readonly >
        </div>

    </div>                             

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary key string</strong></div>

        <div class="col-sm-4 col-md-8">
        <input type="text" class="form-control" name="dictionaryKeyString" id="dictionaryKeyString" >
            <cfif prc.errors.keyExists( "dictionaryKeyString" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryKeyString#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary key bundle</strong></div>

        <div class="col-sm-4 col-md-8">
        <select id="dictionaryKeyBundleCD" class="form-control custom-select" name="dictionaryKeyBundleCD" >
            <cfloop array= #prc.bundles# index="bundle">	
            <option value="#bundle.getBundleCD()#">#bundle.getBundleCD()#&nbsp;|&nbsp;#bundle.getBundleName()#</option>
            </cfloop>
        </select>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary group</strong></div>

        <div class="col-sm-4 col-md-8">
        <select id="dictionaryKeyGroupID" class="form-control custom-select" name="dictionaryKeyGroupID" >
            <cfloop array= #prc.dictionaryGroups# index="dictionaryGroup">	
            <option value="#dictionaryGroup.getDictionaryGroupID()#">#dictionaryGroup.getDictionaryGroupID()#&nbsp;|&nbsp;#dictionaryGroup.getDictionaryGroupName()#</option>
            </cfloop>
        </select>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Source language</strong></div>

        <div class="col-sm-4 col-md-8">
        <select id="dictionaryKeySourceLanguageCD" class="form-control custom-select" name="dictionaryKeySourceLanguageCD" >
            <option value="EN" >EN&nbsp;|&nbsp;ENGLISH</option>
        </select>    
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary key source translation</strong></div>

        <div class="col-sm-4 col-md-8">
        <input type="text" class="form-control" name="dictionaryKeySourceTranslation" id="dictionaryKeySourceTranslation" >
            <cfif prc.errors.keyExists( "dictionaryKeySourceTranslation" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryKeySourceTranslation#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-4 col-form-label my-0"><strong>Dictionary UI key</strong></div>

        <div class="col-sm-4 col-md-8">
        <input type="text" class="form-control" name="dictionaryUIKey" id="dictionaryUIKey" >
            <cfif prc.errors.keyExists( "dictionaryUIKey" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.dictionaryUIKey#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>                      
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "globalization.dictionaryKeys?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>