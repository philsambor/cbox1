<!--- Views/dictionaryKeys/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "Dictionary key details" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Dictionary key string</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.dictionaryKey.getDictionaryKeyString()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Dictionary key bundle</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.dictionaryKey.getDictionaryKeyBundleCD()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Dictionary group</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.dictionaryKey.getDictionaryKeyGroupID()#&nbsp;|&nbsp;#prc.dictionaryKey.getDictionaryGroup().getDictionaryGroupName()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Source language</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.dictionaryKey.getDictionaryKeySourceLanguageCD()#&nbsp;|&nbsp;#prc.dictionaryKey.getLanguage().getLanguageName()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Dictionary key source translation</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.dictionaryKey.getDictionaryKeySourceTranslation()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Dictionary UI key</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.dictionaryKey.getDictionaryUIKey()#</div>
        </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("globalization.dictionaryKeys?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">Back</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>