<!--- Views/dictionaryKeys/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Create a new dictionaryKey" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "dictionaryKeys/_formDictionaryKey", {
    "method" : "POST",
    "action" : event.buildlink( "globalization.dictionaryKeys?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>