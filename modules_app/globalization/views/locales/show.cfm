<!--- Views/locales/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "Locale details" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Locale code</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.locale.getLocaleCD()#</div>
        </div>

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Language code</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.locale.getLocaleLanguageCD()# | #prc.locale.getLanguage().getLanguageName()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Locale name</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.locale.getLocaleName()#</div>
        </div>

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Calendar language</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.locale.getCalendarLang()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

    <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>is Gregorian Calendar?</strong></div>
    <div class="col-sm-4 col-md-8">

        <cfif NOT prc.locale.getIsGregorianCalendar() >          		
            <cfset gregorianCalendar = "No" />  		
        <cfelse>	
            <cfset gregorianCalendar = "Yes" /> 
        </cfif>
        <div class="form-control" readonly>#gregorianCalendar#</div>

    </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("globalization.locales?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">Back</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>