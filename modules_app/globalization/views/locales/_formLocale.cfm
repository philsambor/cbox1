<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <!---<input type="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>Locale code</strong></div>

        <div class="col-sm-4">
        <input type="text" 
               placeholder="en_US" 
               class="form-control" 
               name="LocaleCD" 
               value="#prc.locale.getLocaleCD()#"
               id="localeCD" 
               readonly >
        </div>

        <div class="col-sm-2 col-form-label my-0"><strong>Language code</strong></div>

        <div class="col-sm-4">
            <select id="localelanguageCD" class="form-control custom-select" name="LocaleLanguageCD" >
                <option value="#prc.filter#" selected="selected">#prc.filter#&nbsp;|&nbsp;#prc.filterName#</option>
                <cfloop array= #prc.filterLanguages# index="language">	
                <option value="#language.getLanguageCD()#">#language.getLanguageCD()#&nbsp;|&nbsp;#language.getLanguageName()#</option>
                </cfloop>
       		</select>      
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>Locale name</strong></div>
        <div class="col-sm-4">    
            <input type="text" 
                   class="form-control" 
                   name="localeName" 
                   value="#prc.locale.getLocaleName()#" 
                   id="localeName" >
            <cfif prc.errors.keyExists( "localeName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.localeName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

        <div class="col-sm-2 col-form-label my-0"><strong>Calendar language</strong></div>
        <div class="col-sm-4">
            <input type="text" 
                   class="form-control" 
                   name="calendarLang" 
                   value="#prc.locale.getCalendarLang()#"
                   id="calendarLang" >
            <cfif prc.errors.keyExists( "calendarLang" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.calendarLang#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>       
        </div>

    </div>    
       
    <div class="form-group row">
        
        <div class="col-sm-2 col-form-label my-0"><strong>Gregorian calendar?</strong></div>

            <!--- Based on the value passed by the handler, test the current radio button status --->	
                  
            <cfset varTest = prc.isGregorianCalendar />

            <cfif varTest>
                <cfset checkStatus1 = "checked" />
                <cfset checkStatus2 = "" />   	
            <cfelse>
                <cfset checkStatus1 = "" /> 
                <cfset checkStatus2 = "checked" />   
            </cfif>

            <!--- check or uncheck the radio switch accordingly --->

            <div class="col-sm-4 my-2">

                <label class="form-check form-check-inline">
                    <input type="radio" value="1" name="isGregorianCalendar" #checkStatus1#>
                    <i class="theme"></i>
                    &nbsp;Yes
                </label>

                <label class="form-check form-check-inline">
                    <input type="radio" value="0" name="isGregorianCalendar" #checkStatus2#>
                    <i class="theme no-icon"></i>
                    &nbsp;No
                </label>

            </div>      
    </div>      

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

    <div class="col-sm-2 col-form-label my-0"><strong>Locale code</strong></div>

        <div class="col-sm-4">
        <input type="text" placeholder="en_US" class="form-control" name="LocaleCD" id="localeCD" >
            <cfif prc.errors.keyExists( "localeCD" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.localeCD#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    <div class="col-sm-2 col-form-label my-0"><strong>Language code</strong></div>      
        
        <div class="col-sm-4">
              <select id="localeLanguageCD" class="form-control custom-select" name="LocaleLanguageCD" >
                <option value="#prc.defaultLanguageCode#" selected="selected">#prc.defaultLanguageCode#&nbsp;|&nbsp;#prc.defaultLanguageName#</option>
                <cfloop array= #prc.filterLanguages# index="language">	
                <option value="#language.getLanguageCD()#">#language.getLanguageCD()#&nbsp;|&nbsp;#language.getLanguageName()#</option>
                </cfloop>
       		 </select>
        </div>

    </div>

    <div class="form-group row">

    <div class="col-sm-2 col-form-label my-0"><strong>Locale name</strong></div>

        <div class="col-sm-4">
        <input type="text" class="form-control" name="localeName" id="localeName" >
            <cfif prc.errors.keyExists( "localeName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.localeName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    <div class="col-sm-2 col-form-label my-0"><strong>Calendar language</strong></div>
            
        <div class="col-sm-4">
            <input type="text" class="form-control" name="calendarLang" id="calendarLang" >
            <cfif prc.errors.keyExists( "calendarLang" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.calendarLang#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>
        
    <div class="form-group row">
        
        <div class="col-sm-2 col-form-label my-0"><strong>Gregorian calendar?</strong></div>

            <!--- when this boolean value changes, toggle the radio button visual ---> 
            
            <cfif event.getValue("isGregorianCalendar") >
                <cfset checkStatus1 = "checked" />
                <cfset checkStatus2 = "" />   	
                <cfelse>
                <cfset checkStatus1 = "" /> 
                <cfset checkStatus2 = "checked" />   
            </cfif>

            <!--- check or uncheck the radio switch accordingly --->

               <div class="col-sm-4 my-2">

                  <label class="form-check form-check-inline">
                     <input type="radio" value="1" name="isGregorianCalendar" #checkStatus1#>
                     <i class="theme"></i>
                     &nbsp;Yes
                  </label>

                  <label class="form-check form-check-inline">
                     <input type="radio" value="0" name="isGregorianCalendar" #checkStatus2#>
                     <i class="theme no-icon"></i>
                     &nbsp;No
                  </label>

            </div>      
        </div>
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "globalization.locales?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>