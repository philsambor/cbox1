<!--- Views/locales/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of locales" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('globalization.locales.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.locales.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available locales</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th class="text-center">Locale code</th>
                <th class="text-center">Language code</th>
                <th scope="col">Locale name</th>
                <th class="text-center">Calendar</th>
                <th class="text-center">isGregorian?</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.locales#" index="locale">
            <tr>
                <td class="text-center"><a href="#event.buildlink("globalization.locales.#locale.getLocaleCD()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td class="text-center">#locale.getLocaleCD()#</td>
                <td class="text-center">#locale.getLocaleLanguageCD()#</td>
                <td >#locale.getLocaleName()#</td>
                <td class="text-center">#locale.getCalendarLang()#</td>
                <cfif NOT locale.getIsGregorianCalendar() >          		
                    <cfset gregorianCalendar = "No" />
                    <td class="text-center"><span class="badge badge-warning">#gregorianCalendar#</span></td>  		
                    <cfelse>	
                    <cfset gregorianCalendar = "Yes" />
                    <td class="text-center"><span class="badge badge-primary">#gregorianCalendar#</span></td> 
                    </cfif>  
                <td class="text-center"><a href="#event.buildlink("globalization.locales.#locale.getLocaleCD()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>