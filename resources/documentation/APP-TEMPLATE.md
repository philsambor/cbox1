# Coldbox application template installation and configuration

We are starting with a clean repository called `cbox1` that only contains a README.md file.
(All commands assume we are in the `box` shell unless stated otherwise.)

## Pre-requisites

- CommandBox shell must be installed.
- MySQL 5.7 must have an empty database called `cbox1`
- Create an empty `cbox1` local project folder
- Open VS Code on the `cbox1` project folder

Ideally, a Git repository client such as SourceTree could be installed
to link the remote repository with the local cbox1 project folder

NB: MySQL 5.8 (a.k.a MySQL8) has not been tested for our project. Therefore we recommend to stick to MySQL 5.7

- We will release a documentation update at a later stage for Coldbox 6 and MySQL 8 on MacOS11 (BigSur)
- Meanwhile you may have noticed that MySQL 5.7 downloads are no longer available for MacOS from the MySQL site.

If you need a MySQL 5.7 installation, an alternative option is available. Just use the command:

**brew install mysql@5.7**

and follow the installation instructions from there. We successfully tested this option on MacOS Catalina.

```
Read the PRE-REQUISITES.md document and proceed with all the necessary 
configuration steps for your environment first.

```

## 1 - Generate the skeleton app and finalise the app environment

Open VS Code on the cbox1 project and launch a terminal

### 1.1 - Start the CommandBox shell.

```
// Go to your folder
cd cbox1

// Start the commandBox shell within your folder by typing: box

```

### 1.2  Install Coldbox with a quick-with-auth template

At the commandbox shell, enter the following command:

```
coldbox create app skeleton=cbtemplate-quick-with-auth
```
At the time of update, this will install a base project using **Coldbox 6.x** framework with **Testbox 4**

NB: The tutorial was built earlier on and, at that time, the template was installing Coldbox 5.6.2 and Testbox 3.2.0.
You'll have some differences to take into consideration when updating the config/Coldbox.cfc file.

### 1.3 Add or modify defaulted parameters

*** In config/Coldbox.cfc

Add your default grammar (MySQL) to quick module settings:

moduleSettings = {
			quick : { defaultGrammar = "MySQLGrammar@qb"} // Add this to module settings
		};

With Coldbox 6.2.1, in the Development environment (line 120), the custom error template has changed and should read:

**/coldbox/system/exceptions/Whoops.cfm** instead of /coldbox/system/includes/bugreport.cfm

*** Application.cfc comes pre-populated with a datasource that must be changed :

```sh

this.datasource = "coldbox";   // Must be modified to the actual datasource: "cbox1"

```
### 1.4  Update the .env file

By default, the .env file is pre-populated with information that need to be modified in order for the application to properly start. The same way, this.datasource = "cbox1" change must have been applied to Application.cfc file, we need to update the following in .env:

- APPNAME=cbox1
- DB_CONNECTIONSTRING=jdbc:mysql://127.0.0.1:3306/cbox1?.... // Replace the db name in the connection string
- DB_DATABASE=cbox1
- DB_SCHEMA=cbox1
- DB_PASSWORD=   		// Define the root password to access your database here

Ensure that the .env has been added to .gitignore so that no secrets or credentials find their way to your remote repository.

## 2 - CFML Server

## 2.1 - Start up a local Lucee server

Now we are going to launch a Lucee CFML engine. The following command will start the latest stable Lucee 5 server version.

```sh
start cfengine=lucee@5
```
Now run the following from the commandbox shell:

```sh
server list
```
Take note of the parameters listed for the server cbox1:

- `<server version`: something like `lucee@5.3.6+31`, which varies with the currently available and stable version of the Lucee CFML engine
- `<Portnumber>`: somehing like `50958` which also depends on your own installation 

We need to pin our server engine version, otherwise commandbox will always upgrade with the latest stable version available on ForgeBox, the next time you start your server. This may not be what we want. We are therefore going to pin the CFML server version to: `<server-version>`. 
Note the port number on which the Lucee server was started. You are going to need it next.

### 2.2 Edit server.json to pin your server to a port, an engine and a version

Make a backup of the server.json file before modifying it.

```sh
{
    "app":{
        "cfengine":"<server-version”
    },
    "web":{
        "rewrites":{
            "enable":true
        },
        "http":{
            "port":<Portnumber>
        }
    },
    "name":"cbox1"
}

```

### 2.3 Stop, then re-start your server

```sh
stop or (server stop)
start or (server start)
server list

```
- In the server list output, verify that the server now boots your selected port and engine version.
- When this is fine, delete the server.json backup file

## 3 - Database

## 3.1 - Pre-requisites

Your MySQL database and MySQL client **should have already been installed**...

```sh

You must have an empty MySQL database called `cbox1` already created

```
### 3.2 - Create a password to access Lucee server administration

If you exited the Commandbox shell, open a terminal session and re-start it:

```sh
cd cbox1
box
start or (server start)
```

Within the Commandbox shell, ensure that your Lucee server is started:

```sh
server list
```

You should see a little Lucee icon on your browser top bar.

- Launch the Lucee administrator

```sh
- Click the little Lucee icon on your browser top bar
- Go and select Open > Server Admin from the menu options
```
The latest versions of Lucee will require that a password be first set to access the server administration, either in a password file, which was the only way until Commandbox recently made it easy with cfconfig. Ensure that you have a recent version of the **commandbox-cfconfig** installed. In doubt, grab a fresh version of the module:

install commandbox-cfconfig

Then set a password for the Lucee administrator:

cfconfig set adminPassword=cbox1

### 3.3 - Verify Datasource / MySQL connection

Now you can get back to the Lucee server administrator and use the password you just created to login.

- Once in the administrator, go to Services > Datasource.
- The "cbox1" datasource should have already been created by the Lucee engine based on the .cfconfig.json  file settings which are read when the server re-starts. The .cfconfig.json reads its database connection parameters from the .env file.
- The "testing" datasource should also have been created and should map to the H2 Java SQL database.

## 4 - Review the template installation

The "cbtemplate-quick-with-auth" template installation comes complete with the following modules:

- BCrypt                  // for encryption
- cbauth                  // for authentication
- cbguard                 // to manage secured access
- cbvalidation            // to validate data against pre-set rules
- cfmigrations            // to version control the database schema
- quick                   // to implement native CFML Object Relational Mapping to the database
- redirectBack            // to put the last request on the flash storage
- verify-csrf-interceptor // to prevent cross-site request forgery while submitting forms

Other components were also added by default to enable a login/logout and registration process, such as:

- models/entities/User.cfc  // User entity with properties matching the "users" table
- resources/database/migrations/create_users_table.cfc // A schema builder component to create the user table
- models/validators/UniqueInDatabase

As well as:

- commandbox-dotenv
- commandbox-cfconfig
- commandbox-migrations
- commandbox-cfformat

The following handlers have been created by default:

- handlers/Main.cfc
- handlers/Registrations.cfc
- handlers/Sessions.cfc

* Config/Coldbox.cfc should now have the following module settings overrides:

```sh

moduleSettings = {
			quick : { defaultGrammar = "MySQLGrammar@qb"},
			cbauth : { userServiceClass : "User" },
			cbguard : { authenticationOverrideEvent : "login", authorizationOverrideEvent : "login" }
		};
```
** Config/Router.cfc automatically included the following routes:

```sh

component {

	function configure(){
		// Set Full Rewrites
		setFullRewrites( true );

		route( "/login" ).withHandler( "sessions" ).toAction( { "GET" : "new", "POST" : "create" } );

		route( "/logout", "sessions.delete" );

		resources( resource = "registrations", only = [ "new", "create" ] );

		// Conventions based routing
		route( ":handler/:action?" ).end();
	}

}

```
*** Application.cfc should include the following :

```sh

// Java Integration
this.javaSettings = { loadPaths : [ ".\lib" ], loadColdFusionClassPath : true, reloadOnChange : false };
....
this.datasource = "cbox1"; 
this.mappings[ "/quick" ] = COLDBOX_APP_ROOT_PATH & "/modules/quick";

```
NB: Note that the lib folder should contain a java archive: h2-1.4.199.jar. This archive is the Jdbc driver powering the H2 Java SQL database used for testing.

## 5 - Initiate schema migration

Database migrations are a way of providing version control for your application's database. Changes to database schema are kept in timestamped files that are ran in order `up` and `down`.In the `up` function, you describe the changes to apply your migration. In the `down` function, you describe the changes to undo your migration.

### 5.1 - Create the cfmigrations table

In order to track which migrations have been ran, `cfmigrations` needs to install a table in your database called `cfmigrations`. At the commandbox prompt run the following command:

```sh
> migrate install
```

Go to https://qb.ortusbooks.com/schema-builder/schema-builder to consult the documentation.
See the README.md file found in modules/cfmigrations.

### 5.2 - Migrating the users table up to the database

The default user table schema creation component comes with the template and cna be found in the folder
`resources/database/migrations`. We recommend that you migrate the table as it is, without modifications at that stage. 

Enter the following command at the commandbox prompt:

```sh
> migrate up
```
## 6 - Configure the TESTBOX environment

At the time of update, the quick-with-auth template installs Coldbox 6.2.1 with **Testbox 4.2.1** by default

### 6.1 - Configure TESTBOX source tests folder

By default tests/index.cfm has a root mapping to /testbox/tests/specs. Modify the index file
to have the root mapping point to /tests/specs instead:

```sh
<cfset rootMapping 	= "/tests/specs">
```
Now all tests will be run from the `tests/specs` folder, and all tests will be Git version controlled.

```sh
- cbox1/tests/specs    // put all tests there
- cbox1/tests/results  // ensure that this folder exists
```
NB: The `testbox` directory was automatically added to the .gitignore file. Tests at this location are not version controlled.

NB: Tests are not run against your MySQL database

By default, the tests are run against a H2 Java SQL database mapped to the `testing` datasource. This database is automatically installed in "mode=MySQL" and configured with the template using the driver h2-1.4.199.jar found in the `lib` folder.

** for more on H2: http://www.h2database.com/html/quickstart.html

### 6.2 - Fix missing parameter in Tests configuration file

The tests configuration file is found at tests/resources/**BaseIntegrationSpec.cfc**

In function beforeAll():
// Check if migrations ran before all tests

- Replace: migrationService.setDefaultGrammar( "PostgresGrammar" );
- With: migrationService.setDefaultGrammar( "**PostgresGrammar@qb**" );

Then, reinit the framework: **coldbox reinit**

Missing @qb parameter causes tests to run in error

### 6.3 - Run the tests

Keep in mind that tests are run against a second database instance. Expect query results in tests not to reflect query results that would have otherwise been performed against your development database instance, meaning performed outside your tests.

Go to the tests URL: http://127.0.0.1:<portnumber>/tests

Click on the `Integration` button, then `run All` button. The following tests will be run:

- tests/specs/integration/RegistrationsSpec.cfc
- tests/specs/integration/SessionsSpec.cfc

Both tests should pass and you should now be allowed to:

- Register and login a new user
- Then, logout that user

