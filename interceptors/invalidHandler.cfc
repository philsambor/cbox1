component extends="coldbox.system.Interceptor"{

    function onInvalidEvent(event, interceptData){
        // Log a warning
        log.warn( "Invalid page detected: #arguments.interceptData.invalidEvent#");

        // Set the invalid event to run
        arguments.interceptData.ehBean
            .setHandler("Viewlets")
            .setMethod("routeNotFound");

        // Override
        arguments.interceptData.override = true;
    }
}