<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <!---<input type="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process code</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text"  
               class="form-control" 
               name="ProcessCD" 
               value="#prc.process.getProcessCD()#"
               id="ProcessCD" 
               readonly >
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process dictionary key</strong></div>
        <div class="col-sm-4 col-md-9">
             <select id="processDictionaryKey" class="form-control custom-select" name="processDictionaryKey" >
                <option value="#prc.dictionaryKeyfilter#" selected="selected">#prc.dictionaryKeyfilter#</option>
                <cfloop array= #prc.filterDictionaryKeys# index="dictionaryKey">	
                <option value="#dictionaryKey.getDictionaryKeyString()#">#dictionaryKey.getDictionaryKeyString()#</option>
                </cfloop>
            </select>
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process description</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="processDescription" 
                   value="#prc.process.getProcessDescription()#" 
                   id="processDescription" >
            <cfif prc.errors.keyExists( "processDescription" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.processDescription#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div> 

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process icon</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input type="text" 
                   class="form-control" 
                   name="processIcon" 
                   value="#prc.process.getProcessIcon()#" 
                   id="processIcon" >
        </div>

    </div>          

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process code</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="ProcessCD" id="ProcessCD" >
            <cfif prc.errors.keyExists( "processCD" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.processCD#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process dictionary key</strong></div>

        <div class="col-sm-4 col-md-9">
        <select id="processDictionaryKey" class="form-control custom-select" name="processDictionaryKey" >
            <cfloop array= #prc.processKeys# index="dictionaryKey">	
            <option value="#dictionaryKey.getDictionaryKeyString()#">#dictionaryKey.getDictionaryKeyString()#&nbsp;|&nbsp;#dictionaryKey.getDictionaryKeyBundleCD()#&nbsp;|&nbsp;#dictionaryKey.getDictionaryGroup().getDictionaryGroupName()#</option>
            </cfloop>
       	</select>   
        </div>

    </div> 

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process description</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="processDescription" id="processDescription" >
            <cfif prc.errors.keyExists( "processDescription" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.processDescription#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Process icon</strong></div>

        <div class="col-sm-4 col-md-9">
        <input type="text" class="form-control" name="processIcon" id="processIcon" >
        </div>

    </div>        
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "processes?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>