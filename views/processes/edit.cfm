<!--- Views/processes/edit.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Edit a process" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">

        <p class="m-b btn-groups my-2">
        #html.startForm( method = "DELETE", action = event.buildlink( "processes.#prc.process.getProcessCD()#.seqID.#rc.seqID#") )#
        <button type="submit" name="btn_delete" class="btn btn-outline-danger">Delete</button>
        #html.endForm()#
        </p>

        #renderView( "processes/_formProcess", {
            "method" : "PUT",
            "action" : event.buildlink( "processes.#prc.process.getProcessCD()#.seqID.#rc.seqID#" )
        } )#

    </div>    
</div>



</cfoutput>