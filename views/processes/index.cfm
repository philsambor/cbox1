<!--- Views/processes/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of processes" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('processes.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.processes.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available processes</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Process code</th>
                <th scope="col">Process dictionary key</th>
                <th scope="col">Process description</th>
                <th scope="col">Process icon</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.processes#" index="process">
            <tr>
                <td class="text-center"><a href="#event.buildlink("processes.#process.getProcessCD()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#process.getProcessCD()#</td>
                <td >#process.getProcessDictionarykey()#</td>
                <td >#process.getProcessDescription()#</td>
                <td >#process.getProcessIcon()#</td>
                <td class="text-center"><a href="#event.buildlink("processes.#process.getProcessCD()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>