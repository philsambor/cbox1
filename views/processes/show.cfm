<!--- Views/processes/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "Process details" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Process code</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.process.getProcessCD()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Process dictionary key</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.process.getProcessDictionaryKey()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Process description</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.process.getProcessDescription()#</div>
        </div>
    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-4 col-form-label mt-1"><strong>Process icon</strong></div>
        <div class="col-sm-4 col-md-8">
            <div class="form-control" readonly>#prc.process.getProcessIcon()#</div>
        </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("processes?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">Back</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>