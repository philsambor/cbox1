<cfoutput>

<div class="row">

<!--- Main view --->

    <div class="col-lg-9">

        <div class="h1 mt-3">This is the app_admin/index.cfm view</div>

    </div>

<!---Right bar --->

	<div class="col-lg-3">
		<div class="card card-block mt-3">
			<ul class="card-body nav flex-column bg-light-blue">
				<p><li class="nav-item"><h2>#prc.tsl.menu.myapps#</h2></li></p>
				<cfloop index="i" from="1" to="#arraylen(prc.structNav.appsMenuArray)#">
					<li class="nav-item">
						<cfset app_path = prc.structNav.appsRootPath[i] />
						<a class="nav-link" href="#event.buildLink("#app_path#?seqID=#i#")#">#prc.structNav.appsMenuArray[i]#</a>
					</li>
				</cfloop>
			</ul>
		</div>
	</div>

</div>   

</cfoutput>