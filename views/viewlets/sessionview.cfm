<cfoutput>

<!--- Internationalisation --->

<cfprocessingdirective pageencoding="utf-8">

<!DOCTYPE html>

<!---<html class="no-js" dir="#prc.structULocale.dirText#" lang="#prc.structULocale.languageCode#">--->
<html class="no-js" dir="ltr" lang="en">

<body>

<div class="row">

	<!---Left Bar --->
	<div class="col-lg-3">
		<div class="card text-left card-block mx-1 mt-3">
			<nav>
			<ul class="card-body nav flex-column bg-light">
				<p><li class="nav-item"><h2>#prc.tsl.menu.menu_options#</h2></li></p>
				<cfloop index="i" from="1" to="#arraylen(prc.arrayAppSubmenu[prc.seqID])#">
					<li class="nav-item">
						<a class="nav-link" href="#event.buildLink("#prc.arrayAppSubmenu[prc.seqID][i][2]#?seqID=#prc.seqID#")#">#prc.arrayAppSubmenu[prc.seqID][i][1]#</a>
					</li>
				</cfloop>
			</ul>
		</div>
	</div>

	<!--- Main view --->

	<div class="col-lg-9">

		<div class="container">#renderView()#</div>
		
	</div>

</div>
          

</body>
</html>
</cfoutput>
