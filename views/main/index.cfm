<cfoutput>

<div class="row">

	<!--- Main view --->

	<div class="col-lg-9">

		<cfif structCount( getSetting("Modules") )>
			<section id="modules">
			<div class="pb-2 mt-5 mb-2 border-bottom">
		        <h2>Registered modules</h2>
			</div>
			<p>Below are your application's loaded modules.</p>
			<ul>
				<cfloop collection="#getSetting("Modules")#" item="thisModule">
				<li><a href="#event.buildLink( getModuleConfig( thisModule ).inheritedEntryPoint )#">#thisModule#</a></li>
				</cfloop>
			</ul>
			</section>
		</cfif>
		
	</div>

	<!---Right bar --->

	<div class="col-lg-3">
		<div class="card card-block mt-3">
			<ul class="card-body nav flex-column bg-light-blue">
				<p><li class="nav-item"><h2>#prc.tsl.menu.myapps#</h2></li></p>
				<cfloop index="i" from="1" to="#arraylen(prc.structNav.appsMenuArray)#">
					<li class="nav-item">
						<cfset app_path = prc.structNav.appsRootPath[i] />
						<a class="nav-link" href="#event.buildLink("#app_path#?seqID=#i#")#">#prc.structNav.appsMenuArray[i]#</a>
					</li>
				</cfloop>
			</ul>
		</div>
	</div>

</div>
</cfoutput>