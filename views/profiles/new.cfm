<!--- Views/profiles/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Create a new profile" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "profiles/_formProfile", {
    "method" : "POST",
    "action" : event.buildlink( "profiles?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>