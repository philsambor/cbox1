<!--- Views/profiles/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of profiles" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('profiles.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.profiles.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available profiles</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Process</th>
                <th scope="col">Profile name</th>
                <th scope="col">Role</th>
                <th scope="col">Scope</th>
                <th scope="col">Profile description</th>
            <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.profiles#" index="profile">
            <tr>
                <td class="text-center"><a href="#event.buildlink("profiles.#profile.getProfileID()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#profile.getProfileProcessCD()#</td>
                <td >#profile.getProfileName()#</td>
                <td >#profile.getProfileRoleName()#</td>
                <td >#profile.getProfileScopeID()#</td>
                <td >#profile.getProfileDescription()#</td>
                <td class="text-center"><a href="#event.buildlink("profiles.#profile.getProfileID()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>