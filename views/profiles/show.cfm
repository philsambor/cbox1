<!--- Views/profiles/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "Profile details" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile ID</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profile.getProfileID()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile name</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profile.getProfileName()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile process code</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profile.getProfileProcessCD()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile role name</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profile.getProfileRoleName()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile scope</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profile.getProfileScopeID()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile description</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profile.getProfileDescription()#</div>
        </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("profiles?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">Back</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>