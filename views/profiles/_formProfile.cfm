<cfoutput>

#html.startForm( method = args.method, action = args.action )#

   <!--- <input Context="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile name</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input Context="text" 
                   class="form-control" 
                   name="profileName" 
                   value="#prc.profile.getProfileName()#" 
                   id="profileName" >
            <cfif prc.errors.keyExists( "profileName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.profileName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile process code</strong></div>
        <div class="col-sm-4 col-md-9">
            <select id="profileProcessCD" class="form-control custom-select" name="profileProcessCD" readonly >
                <option value="#prc.filterProcess#" selected="selected">#prc.filterProcess#&nbsp;|&nbsp;#prc.filterProcessName#</option>
            </select>       
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile role name</strong></div>
        <div class="col-sm-4 col-md-9">
            <select id="profileRoleName" class="form-control custom-select" name="profileRoleName" >
                <option value="#prc.filterRole#" selected="selected">#prc.filterRole#&nbsp;|&nbsp;#prc.filterRoleDescription#</option>
                <cfloop array= #prc.filterRoles# index="role">	
                <option value="#role.getRoleName()#">#role.getRoleName()#&nbsp;|&nbsp;(#role.getRoleDescription()#)</option>
                </cfloop>
            </select>    

        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope</strong></div>
        <div class="col-sm-4 col-md-9">
            <select id="profileScopeID" class="form-control custom-select" name="profileScopeID" >
                <option value="#prc.filterScope#" selected="selected">#prc.filterScope#&nbsp;|&nbsp;#prc.filterScopeName#</option>
                <cfloop array= #prc.filterProfileScopes# index="profileScope">	
                <option value="#profileScope.getProfileScopeID()#">#profileScope.getProfileScopeID()#&nbsp;|&nbsp;(#profileScope.getProfileScopeName()#)</option>
                </cfloop>
            </select>
            
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile description</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input Context="text" 
                   class="form-control" 
                   name="profileDescription" 
                   value="#prc.profile.getProfileDescription()#" 
                   id="profileDescription" >
        </div>

    </div>               

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile name</strong></div>

        <div class="col-sm-4 col-md-9">
        <input Context="text" class="form-control" name="profileName" id="profileName" >
            <cfif prc.errors.keyExists( "profileName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.profileName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile process code</strong></div>

        <div class="col-sm-4 col-md-9">
        <select id="profileProcessCD" class="form-control custom-select" name="profileProcessCD" >
            <cfloop array= #prc.processes# index="process">	
            <option value="#process.getProcessCD()#">#process.getProcessCD()#&nbsp;|&nbsp;(#process.getProcessDescription()#)</option>
       	    </cfloop>
        </select>    
        
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile role name</strong></div>

        <div class="col-sm-4 col-md-9">
        <select id="profileRoleName" class="form-control custom-select" name="profileRoleName" >
            <cfloop array= #prc.roles# index="role">	
            <option value="#role.getRoleName()#">#role.getRoleName()#&nbsp;|&nbsp;(#role.getRoleDescription()#)</option>
       	    </cfloop>
       	</select>    
        
        </div>

    </div> 

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope</strong></div>

        <div class="col-sm-4 col-md-9">

        <select id="profileScopeID" class="form-control custom-select" name="profileScopeID" >
            <cfloop array= #prc.profileScopes# index="profileScope">	
            <option value="#profileScope.getProfileScopeID()#">#profileScope.getProfileScopeID()#&nbsp;|&nbsp;(#profileScope.getProfileScopeName()#)</option>
       	    </cfloop>
       	</select> 

        </div>

    </div>      

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile description</strong></div>

        <div class="col-sm-4 col-md-9">
        <input Context="text" class="form-control" name="profileDescription" id="profileDescription" >
        </div>

    </div>    
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button Context="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "profiles?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>