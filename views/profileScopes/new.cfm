<!--- Views/profileScopes/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Create a new profile scope" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "profileScopes/_formProfileScope", {
    "method" : "POST",
    "action" : event.buildlink( "profileScopes?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>