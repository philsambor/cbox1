<cfoutput>

#html.startForm( method = args.method, action = args.action )#

   <!--- <input Context="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope name</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input Context="text" 
                   class="form-control" 
                   name="profileScopeName" 
                   value="#prc.profileScope.getProfileScopeName()#" 
                   id="profileScopeName" >
            <cfif prc.errors.keyExists( "profileScopeName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.profileScopeName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope type</strong></div>
        <div class="col-sm-4 col-md-9">
           
           <cfset varType = prc.profileScope.getProfileScopeType() />

            <select class="form-control custom-select"  id="profileScopeType" name="profileScopeType" >

            <cfswitch expression='#varType#' >

                <cfcase value="ANY" >
                    <cfset varTypeLabel = "ANY&nbsp;|&nbsp;IN ANY CONTEXT" />
                    <option value="#varType#" selected="selected">#varTypeLabel#</option>
                    <option value="TRADE" >TRADE&nbsp;|&nbsp;IN A GOODS TRADING CONTEXT</option> 
                    <option value="SERVICE">SERVICE&nbsp;|&nbsp;IN A SERVICE PROVIDER CONTEXT</option>
                </cfcase>
                <cfcase value="TRADE" >
                    <cfset varTypeLabel = "TRADE&nbsp;|&nbsp;IN A GOODS TRADING CONTEXT" />
                    <option value="#varType#" selected="selected">#varTypeLabel#</option>
                    <option value="SERVICE">SERVICE&nbsp;|&nbsp;IN A SERVICE PROVIDER CONTEXT</option>
                    <option value="ANY" >ANY&nbsp;|&nbsp;IN ANY CONTEXT</option> 
                </cfcase>
                <cfcase value="SERVICE" >
                    <cfset varTypeLabel = "SERVICE&nbsp;|&nbsp;IN A SERVICE PROVIDER CONTEXT" />
                    <option value="#varType#" selected="selected">#varTypeLabel#</option>
                    <option value="TRADE" >TRADE&nbsp;|&nbsp;IN A GOODS TRADING CONTEXT</option> 
                    <option value="ANY">ANY&nbsp;|&nbsp;IN ANY CONTEXT</option>
                </cfcase>

            </cfswitch>

            </select>
    
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope context</strong></div>
        <div class="col-sm-4 col-md-9">

         <cfset varContext = prc.profileScope.getProfileScopeContext() />

            <select class="form-control custom-select"  id="profileScopeContext" name="profileScopeContext" >

            <cfswitch expression='#varContext#' >

                <cfcase value="TENANT" >
                    <cfset varContextLabel = "SAAS PLATFORM TENANT" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option> 
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>

                </cfcase>
                <cfcase value="SUPPLIER" >
                    <cfset varContextLabel = "SUPPLIER, EXPORTER (SHIPPER)" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option> 
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
                </cfcase>
                <cfcase value="WHORIGIN" >
                    <cfset varContextLabel = "WAREHOUSE OR PLANT AT ORIGIN" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option> 
                </cfcase>
                <cfcase value="LOGORIGIN" >
                    <cfset varContextLabel = "LOGISTICS AGENT AT ORIGIN" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
                </cfcase>
                <cfcase value="PRINCIPAL" >
                    <cfset varContextLabel = "SUPPLY CHAIN LEAD PRINCIPAL (TRADE)" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
                </cfcase>
                <cfcase value="4PL" >
                    <cfset varContextLabel = "SUPPLY CHAIN LEAD 4PL (SERVICE)" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
                </cfcase>
                <cfcase value="WHDESTINATION" >
                    <cfset varContextLabel = "WAREHOUSE OR PLANT AT DESTINATION" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
                </cfcase>
                <cfcase value="LOGDESTINATION" >
                    <cfset varContextLabel = "LOGISTICS AGENT AT DESTINATION" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
                </cfcase>
                <cfcase value="BUYER" >
                    <cfset varContextLabel = "BUYER, IMPORTER (CONSIGNEE)" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
                </cfcase>
                <cfcase value="PLATFORM" >
                    <cfset varContextLabel = "SAAS PLATFORM ADMINISTRATOR" />
                    <option value="#varContext#" selected="selected">#varContextLabel#</option>
                    <option value="TENANT">SAAS PLATFORM TENANT</option>
                    <option value="SUPPLIER" >SUPPLIER, EXPORTER (SHIPPER)</option>
                    <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
                    <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
                    <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
                    <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
                    <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
                    <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
                    <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
                </cfcase>

            </cfswitch>

            </select>

        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope description</strong></div>
        <div class="col-sm-4 col-md-9">    
            <input Context="text" 
                   class="form-control" 
                   name="profileScopeDescription" 
                   value="#prc.profileScope.getProfileScopeDescription()#" 
                   id="profileScopeDescription" >
        </div>

    </div>               

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope name</strong></div>

        <div class="col-sm-4 col-md-9">
        <input Context="text" class="form-control" name="profileScopeName" id="profileScopeName" >
            <cfif prc.errors.keyExists( "profileScopeName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.profileScopeName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope type</strong></div>

        <div class="col-sm-4 col-md-9">
        <select id="profileScopeType" class="form-control custom-select" name="profileScopeType" >
            <option value="ANY" selected="selected">ANY&nbsp;|&nbsp;IN ANY CONTEXT</option>	
            <option value="TRADE">TRADE&nbsp;|&nbsp;IN A GOODS TRADING CONTEXT</option>
            <option value="SERVICE">SERVICE&nbsp;|&nbsp;IN A SERVICE PROVIDER CONTEXT</option>
       	</select>    
        
        </div>

    </div> 

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope context</strong></div>

        <div class="col-sm-4 col-md-9">

        <select id="profileScopeContext" class="form-control custom-select" name="profileScopeContext" >
            <option value="TENANT" selected="selected">SAAS PLATFORM TENANT</option>
            <option value="SUPPLIER">SUPPLIER, EXPORTER (SHIPPER)</option>	
            <option value="WHORIGIN">WAREHOUSE OR PLANT AT ORIGIN</option>
            <option value="LOGORIGIN">LOGISTICS AGENT AT ORIGIN</option>
            <option value="PRINCIPAL">SUPPLY CHAIN LEAD PRINCIPAL (TRADE)</option>
            <option value="4PL">SUPPLY CHAIN LEAD 4PL (SERVICE)</option>
            <option value="WHDESTINATION">WAREHOUSE OR PLANT AT DESTINATION</option>
            <option value="LOGDESTINATION">LOGISTICS AGENT AT DESTINATION</option>
            <option value="BUYER">BUYER, IMPORTER (CONSIGNEE)</option>
            <option value="PLATFORM">SAAS PLATFORM ADMINISTRATOR</option>
       	</select> 

        </div>

    </div>      

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile scope description</strong></div>

        <div class="col-sm-4 col-md-9">
        <input Context="text" class="form-control" name="profileScopeDescription" id="profileScopeDescription" >
        </div>

    </div>    
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button Context="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "profileScopes?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>