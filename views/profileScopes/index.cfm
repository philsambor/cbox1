<!--- Views/profileScopes/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of profile scopes" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('profileScopes.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.profileScopes.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available profile scopes</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Profile scope name</th>
                <th scope="col">Profile scope type</th>
                <th scope="col">Profile scope context</th>
                <th scope="col">Profile scope description</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.profileScopes#" index="profileScope">
            <tr>
                <td class="text-center"><a href="#event.buildlink("profileScopes.#profileScope.getProfileScopeID()#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#profileScope.getProfileScopeName()#</td>
                <td >#profileScope.getProfileScopeType()#</td>
                <td >#profileScope.getProfileScopeContext()#</td>
                <td >#profileScope.getProfileScopeDescription()#</td>
                <td class="text-center"><a href="#event.buildlink("profileScopes.#profileScope.getProfileScopeID()#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>