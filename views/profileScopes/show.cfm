<!--- Views/profileScopes/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "Profile scope details" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile scope ID</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profileScope.getProfileScopeID()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile scope name</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profileScope.getProfileScopeName()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile scope type</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profileScope.getProfileScopeType()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile scope context</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profileScope.getProfileScopeContext()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Profile scope description</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.profileScope.getProfileScopeDescription()#</div>
        </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("profileScopes?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">Back</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>