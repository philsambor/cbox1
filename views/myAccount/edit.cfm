<cfoutput>

<cfset request.tslFormTitle = "Edit my account" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>


<form method="POST" action="#event.buildLink( "myAccount.update.#prc.user.getID()#.seqID.#rc.seqID#" )#">       
    
    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>Email address</strong></div>

        <div class="col-sm-4">
        <input type="text"  
               class="form-control" 
               name="email" 
               value= #prc.user.getEmail()#
               id="userId"
               readonly >
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>First name</strong></div>

        <div class="col-sm-4">
        <input type="text"  
               class="form-control" 
               name="firstname" 
               value= #prc.user.getFirstname()#
               id="firstname" >
        <cfif prc.errors.keyExists( "firstname" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.firstname#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>       
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>Last name</strong></div>

        <div class="col-sm-4">
        <input type="text"  
               class="form-control" 
               name="lastname" 
               value= #prc.user.getLastname()#
               id="lastname" >
         <cfif prc.errors.keyExists( "lastname" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.lastname#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>       
        </div>
             
        <!--</div>-->

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>My locale</strong></div>
        <div class="col-sm-4">
             <select id="localeCode" class="form-control custom-select" name="localeCode" >
                <option value="#prc.localefilter#" selected="selected">#prc.localefilter#&nbsp;|&nbsp;#prc.localefilterName#</option>
                <cfloop array= #prc.filterLocalesExcept# index="locale">	
                <option value="#locale.getLocaleCD()#">#locale.getLocaleCD()#&nbsp;|&nbsp;#locale.getLocaleName()#</option>
                </cfloop>
            </select>
        </div>

    </div>
    
    <!---<div class="form-group">
    
        <label for="localeCD">My locale:</label>
        
        <select id="localeCode" class="form-control custom-select" name="localeCode" >
            <option value="#prc.localefilter#" selected="selected">#prc.localefilter#&nbsp;|&nbsp;#prc.localefilterName#</option>
            <cfloop array= #prc.filterLocalesExcept# index="locale">	
            <option value="#locale.getLocaleCD()#">#locale.getLocaleCD()#&nbsp;|&nbsp;#locale.getLocaleName()#</option>
            </cfloop>
        </select>

    </div> --->

     <!--- Buttons --->

    <div class="row mt-5">

        <div class="border border-0 col-12">
            <p class="m-b btn-groups my-2">
            <button type="submit" name="btn_save" class="btn btn-primary">Save</button>
            <a href = "#event.buildLink( "main?seqID=#rc.seqID#" )#" name="btn_back" class="btn btn-outline-primary">Back</a>
            </p>
        </div>
                        
    </div>   
        
</form>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>