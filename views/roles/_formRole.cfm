<cfoutput>

#html.startForm( method = args.method, action = args.action )#

    <!---<input type="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>#prc.tsl.screen.ROLE_NAME#</strong></div>

        <div class="col-sm-4">
        <input type="text"  
               class="form-control" 
               name="RoleName" 
               value="#prc.role.getRoleName()#"
               id="roleName" 
               readonly >
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>#prc.tsl.screen.ROLE_DESCRIPTION#</strong></div>
        <div class="col-sm-4">    
            <input type="text" 
                   class="form-control" 
                   name="roleDescription" 
                   value="#prc.role.getRoleDescription()#" 
                   id="roleDescription" >
            <cfif prc.errors.keyExists( "roleDescription" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.roleDescription#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>         

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>#prc.tsl.screen.ROLE_NAME#</strong></div>

        <div class="col-sm-4">
        <input type="text" class="form-control" name="roleName" id="roleName" >
            <cfif prc.errors.keyExists( "roleName" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.roleName#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>  

    <div class="form-group row">

        <div class="col-sm-2 col-form-label my-0"><strong>#prc.tsl.screen.ROLE_DESCRIPTION#</strong></div>

        <div class="col-sm-4">
        <input type="text" class="form-control" name="roleDescription" id="roleDescription" >
            <cfif prc.errors.keyExists( "roleDescription" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.roleDescription#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>    
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button type="submit" name="btn_save" class="btn btn-primary">#prc.tsl.menu.SAVE#</button>
        <a href = "#event.buildLink( "roles?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">#prc.tsl.menu.CANCEL#</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>