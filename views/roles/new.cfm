<!--- Views/roles/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "#prc.tsl.screen.ROLES_ADD#" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "roles/_formRole", {
    "method" : "POST",
    "action" : event.buildlink( "roles?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>