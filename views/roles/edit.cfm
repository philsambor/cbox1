<!--- Views/roles/edit.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "#prc.tsl.screen.ROLES_UPDATE#" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">

        <p class="m-b btn-groups my-2">
        #html.startForm( method = "DELETE", action = event.buildlink( "roles.#prc.role.getRoleName()#.seqID.#rc.seqID#") )#
        <button type="submit" name="btn_delete" class="btn btn-outline-danger">#prc.tsl.menu.DELETE#</button>
        #html.endForm()#
        </p>

        #renderView( "roles/_formRole", {
            "method" : "PUT",
            "action" : event.buildlink( "roles.#prc.role.getRoleName()#.seqID.#rc.seqID#" )
        } )#

    </div>    
</div>

</cfoutput>