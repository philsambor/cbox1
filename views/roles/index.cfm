<!--- Views/roles/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "#prc.tsl.screen.ROLES_LIST#" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('roles.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">#prc.tsl.menu.NEW#</a>
        </p>
    </div>    
</div>       
           
<cfif prc.roles.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available roles</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">#prc.tsl.screen.ROLE_NAME#</th>
                <th scope="col">#prc.tsl.screen.ROLE_DESCRIPTION#</th>
               <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>    
        <tbody>
           <cfloop array="#prc.roles#" index="role">
            <tr>
                <td class="text-center"><a href="#event.buildlink("roles.#role.getRoleName()#.seqID.#rc.seqID#")#" class="card-link">#prc.tsl.menu.VIEW#</a></td>
                <td >#role.getRoleName()#</td>
                <td >#role.getRoleDescription()#</td>
                <td class="text-center"><a href="#event.buildlink("roles.#role.getRoleName()#.edit.seqID.#rc.seqID#")#" class="card-link">#prc.tsl.menu.EDIT#</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>