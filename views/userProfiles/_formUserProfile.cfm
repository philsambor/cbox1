<cfoutput>

#html.startForm( method = args.method, action = args.action )#

   <!--- <input Context="hidden" name="_token" value="#csrfGenerateToken()#" />--->
    
    <!--- when not creating a record --->
    
    <cfif(args.method NEQ "POST") >

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>User</strong></div>
        <div class="col-sm-4 col-md-9">
            <select id="userLoginID" class="form-control custom-select" name="userLoginID" readonly>
            <option value="#prc.filterUser#" selected="selected">#prc.filterUser#&nbsp;|&nbsp;#prc.filterUserEmail#</option>
        </select>        
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>User profile</strong></div>
        <div class="col-sm-4 col-md-9">
            <select id="userProfileID" class="form-control custom-select" name="userProfileID" readonly >
                <option value="#prc.filterProfile#" selected="selected">#prc.filterProfile#&nbsp;|&nbsp;#prc.filterProfileName#</option>
            </select>       
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>User workgroup</strong></div>
        <div class="col-sm-4 col-md-9">
            <input Context="text" 
                   class="form-control" 
                   name="userWorkgroupID" 
                   value="#prc.userProfile.getUserWorkgroupID()#" 
                   id="userWorkgroupID" >
              <cfif prc.errors.keyExists( "userWorkgroupID" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.userWorkgroupID#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>      

        </div>

    </div>

    <div class="form-group row">
        
        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Assign profile?</strong></div>

            <!--- Based on the value passed by the handler, test the current radio button status --->	
                  
            <cfset varTest = prc.userProfileOK />

            <cfif varTest>
                <cfset checkStatus1 = "checked" />
                <cfset checkStatus2 = "" />   	
            <cfelse>
                <cfset checkStatus1 = "" /> 
                <cfset checkStatus2 = "checked" />   
            </cfif>

            <!--- check or uncheck the radio switch accordingly --->

            <div class="col-sm-4 col-md-9 my-2">

                <label class="form-check form-check-inline">
                    <input type="radio" value="1" name="userProfileOK" #checkStatus1#>
                    <i class="theme"></i>
                    &nbsp;Yes
                </label>

                <label class="form-check form-check-inline">
                    <input type="radio" value="0" name="userProfileOK" #checkStatus2#>
                    <i class="theme no-icon"></i>
                    &nbsp;No
                </label>

            </div>      
    </div>              

    <cfelse>

    <!--- in creation mode --->

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>User</strong></div>

        <div class="col-sm-4 col-md-9">
        <select id="userLoginID" class="form-control custom-select" name="userLoginID" >
            <cfloop array= #prc.users# index="user">	
            <option value="#user.getID()#">#user.getID()#&nbsp;|&nbsp;(#user.getEmail()#)</option>
       	    </cfloop>
        </select>    
        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Profile</strong></div>

        <div class="col-sm-4 col-md-9">
        <select id="userProfileID" class="form-control custom-select" name="userProfileID" >
            <cfloop array= #prc.profiles# index="profile">	
            <option value="#profile.getProfileID()#">#profile.getProfileID()#&nbsp;|&nbsp;(#profile.getProfileName()#)</option>
       	    </cfloop>
        </select>    
        
        </div>

    </div>

     <div class="form-group row">

        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>User workgroup</strong></div>

        <div class="col-sm-4 col-md-9">
        <input Context="text" class="form-control" name="userWorkgroupID" id="userWorkgroupID" >
            <cfif prc.errors.keyExists( "userWorkgroupID" )>
                <small class="form-text text-danger">
                    <cfloop array="#prc.errors.userWorkgroupID#" index="error">
                        <p>#error.message#</p>
                    </cfloop>
                </small>
            </cfif>
        </div>

    </div>

    <div class="form-group row">
        
        <div class="col-sm-2 col-md-3 col-form-label my-0"><strong>Assign profile?</strong></div>

            <!--- when this boolean value changes, toggle the radio button visual ---> 
            
            <cfif event.getValue("userProfileOK") >
                <cfset checkStatus1 = "checked" />
                <cfset checkStatus2 = "" />   	
                <cfelse>
                <cfset checkStatus1 = "" /> 
                <cfset checkStatus2 = "checked" />   
            </cfif>

            <!--- check or uncheck the radio switch accordingly --->

               <div class="col-sm-4 col-md-9 my-2">

                  <label class="form-check form-check-inline">
                     <input type="radio" value="1" name="userProfileOK" #checkStatus1#>
                     <i class="theme"></i>
                     &nbsp;Yes
                  </label>

                  <label class="form-check form-check-inline">
                     <input type="radio" value="0" name="userProfileOK" #checkStatus2#>
                     <i class="theme no-icon"></i>
                     &nbsp;No
                  </label>

            </div>      
        </div> 
        
    </cfif>  

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <button Context="submit" name="btn_save" class="btn btn-primary">Save</button>
        <a href = "#event.buildLink( "userProfiles?seqID=#rc.seqID#" )#" name="btn_cancel" class="btn btn-outline-primary">Cancel</a>
        </p>
    </div>
                    
</div>

#html.endForm()#

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>