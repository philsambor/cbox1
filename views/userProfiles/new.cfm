<!--- Views/userProfiles/new.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Create a new user profile" />

<!--- screen title ---> 
<p><h2>#request.tslFormTitle #</h2></p>

<div class="row mt-5">
    <div class="border border-0 col-12">

      #renderView( "userProfiles/_formUserProfile", {
    "method" : "POST",
    "action" : event.buildlink( "userProfiles?seqID=#rc.seqID#" )
    } )#

    </div>    
</div>

</cfoutput>