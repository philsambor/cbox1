<!--- Views/userProfiles/index.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "List of user profiles" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12">
        <p class="m-b btn-groups my-2">
        <a href="#event.buildLink('userProfiles.new.seqID.#rc.seqID#')#" name="btn_new" class="btn btn-primary">New</a>
        </p>
    </div>    
</div>       
           
<cfif prc.userProfiles.isEmpty() >
   <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">No available user profiles</p> 
        </div>
   </div>
    <cfelse>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th class="text-center"><span class="fa fa-pencil"></span>
                <th scope="col">Login ID</th>
                <th scope="col">Profile ID</th>
                <th class="text-center">Workgroup ID</th>
                <th class="text-center">Profile assigned</th>
            <th class="text-center"><span class="fa fa-pencil"></span></th>
            </tr>
        </thead>
        <tbody>
           <cfloop array="#prc.userProfiles#" index="userProfile">
           <cfset var userLoginID = userProfile.getUserLoginID() />
           <cfset var userProfileID = userProfile.getUserProfileID() /> 
            <tr>
                <td class="text-center"><a href="#event.buildlink("userProfiles.#userLoginID#.#userProfileID#.seqID.#rc.seqID#")#" class="card-link">Read</a></td>
                <td >#userProfile.getUserLoginID()#&nbsp;|&nbsp;#userProfile.getUser().getEmail()#</td>
                <td >#userProfile.getUserProfileID()#&nbsp;|&nbsp;#userProfile.getProfile().getProfileName()#</td>
                <td class="text-center" >#userProfile.getUserWorkgroupID()#</td>
                <cfif NOT userProfile.getUserProfileOk() >          		
                    <cfset assignedFlag = "No" />   
                    <td class="text-center"><span class="badge badge-warning">#assignedFlag#</span></td>  		
                    <cfelse>	
                    <cfset assignedFlag = "Yes" />
                    <td class="text-center"><span class="badge badge-success">#assignedFlag#</span></td>   
                </cfif>  
                <td class="text-center"><a href="#event.buildlink("userProfiles.#userLoginID#.#userProfileID#.edit.seqID.#rc.seqID#")#" class="card-link">Edit</a></td>
            </tr>
           </cfloop>
        </tbody>
    </table>

</cfif>

<p class="container">
<div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>


</cfoutput>