<!--- Views/userProfiles/show.cfm --->

<cfoutput>

<!--- Display translated form title --->

<cfset request.tslFormTitle = "User profile details" />

<!--- screen title ---> 

<p><h1>#request.tslFormTitle #</h1></p>

<div class="row mt-3">
    <div class="border border-0 col-12"><p></p></div>    
</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>User</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.userProfile.getUserLoginID()#&nbsp;|&nbsp;#prc.userProfile.getUser().getNickname()#&nbsp;|&nbsp;#prc.userProfile.getUser().getEmail()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>User profile</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.userProfile.getUserProfileID()#&nbsp;|&nbsp;#prc.userProfile.getProfile().getProfileName()#</div>
        </div>

</div>

<div class="form-group row my-0">

        <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>User workgroup</strong></div>
        <div class="col-sm-4 col-md-9">
            <div class="form-control" readonly>#prc.userProfile.getUserWorkgroupID()#</div>
        </div>

</div>

<div class="form-group row my-0">

    <div class="col-sm-2 col-md-3 col-form-label mt-1"><strong>Assigned?</strong></div>
    <div class="col-sm-4 col-md-9">

        <cfif NOT prc.userProfile.getUserProfileOK() >          		
            <cfset assigned = "No" />  		
        <cfelse>	
            <cfset assigned = "Yes" /> 
        </cfif>
        <div class="form-control" readonly>#assigned#</div>

    </div>
    
</div>

<!--- Buttons --->

<div class="row mt-5">

    <div class="border border-0 col-12">

    <p class="m-b btn-groups my-2">
    <a href="#event.buildlink("userProfiles?seqID=#rc.seqID#")#" name="btn_back" class="btn btn-outline-primary">Back</a>
    </p>

    </div>
                        
</div>

<p class="container">
    <div class="text-dark bg-warning">#getInstance( "MessageBox@cbmessagebox" ).renderIt()#</div>  
</p>

</cfoutput>