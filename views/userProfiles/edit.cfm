<!--- Views/userProfiles/edit.cfm --->

<cfoutput>

<!--- Display translated form title --->
<cfset request.tslFormTitle = "Edit a user profile" />

<!--- screen title --->
<p><h1>#request.tslFormTitle#</h1></p>

<cfset userLoginID = prc.userProfile.getUserLoginID() />
<cfset userProfileID = prc.userProfile.getUserProfileID() /> 

<div class="row mt-3">
    <div class="border border-0 col-12">

        <p class="m-b btn-groups my-2">
        #html.startForm( method = "DELETE", action = event.buildlink( "userProfiles.#userLoginID#.#userProfileID#.seqID.#rc.seqID#") )#
        <button type="submit" name="btn_delete" class="btn btn-outline-danger">Delete</button>
        #html.endForm()#
        </p>

        #renderView( "userProfiles/_formUserProfile", {
            "method" : "PUT",
            "action" : event.buildlink( "userProfiles.#userLoginID#.#userProfileID#.seqID.#rc.seqID#" )
        } )#

    </div>    
</div>



</cfoutput>