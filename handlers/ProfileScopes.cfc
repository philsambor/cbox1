/**
* I am the ProfileScopes handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  // prc.profileScopes = getInstance( "ProfileScope" ).all();

    prc.profileScopes = getInstance( "ProfileScope" )
      .orderBy( "profileScopeContext" )
      .get();

     event.setView( "profileScopes/index");   

    }

 function show(event, rc, prc)  {

  prc.profileScope = getInstance( "profileScope" ).findOrFail(rc.profileScopeID);
  event.setView( "profileScopes/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "profileScope_form_errors", {} ); 

  event.setView( "profileScopes/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
	      "profileScopeName" : { "required" : true }
	      //"profileScopeType" : { "required" : true },
        //"profileScopeContext" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "profileScope_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var profileScope = getInstance( "profileScope" ).create( {
    "profileScopeName" : Ucase(rc.profileScopeName),
    "profileScopeType" : Ucase(rc.profileScopeType),
    "profileScopeContext" : Ucase(rc.profileScopeContext),
    "profileScopeDescription" : Ucase(rc.profileScopeDescription)
    });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Profile scope created successfully!" );
  relocate( "profileScopes?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.profileScope = getInstance( "profileScope" ).findOrFail(rc.profileScopeID);
  param prc.errors = flash.get( "profileScope_form_errors", {} ); 

  event.setView( "profileScopes/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "profileScopeName" : { "required" : true }
	     // "profileScopeType" : { "required" : true },
       // "profileScopeContext" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "profileScope_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var profileScope = getInstance( "profileScope" ).findOrFail(rc.profileScopeID);
  profileScope.setProfileScopeName( Ucase(rc.profileScopeName) );
  profileScope.setProfileScopeType( rc.profileScopeType);
  profileScope.setProfileScopeContext( rc.profileScopeContext);
  profileScope.setProfileScopeDescription( Ucase(rc.profileScopeDescription) );
  profileScope.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Profile scope updated successfully!" );
  relocate( "profileScopes.#profileScope.getProfileScopeID()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var profileScope = getInstance( "profileScope" ).findOrFail(rc.profileScopeID);

  try {

  profileScope.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Profile scope deleted successfully!" );
  relocate( "profileScopes?seqID=#rc.seqID#" );

  }


}