/**
* I am the Roles handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  prc.roles = getInstance( "Role" ).all();

  // prc.roles = getInstance( "Role" )
  //   .orderBy( "roleName" )
  //   .get();

  event.setView( "roles/index");  

  }

 function show(event, rc, prc)  {

  prc.role = getInstance( "Role" ).findOrFail(rc.roleName);
  event.setView( "roles/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "role_form_errors", {} ); 

  event.setView( "roles/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "roleName" : { "required" : true },
				"roleDescription" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "role_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var role = getInstance( "Role" ).create( {
    "roleName" : Ucase(rc.roleName),
    "roleDescription" : Ucase(rc.roleDescription)
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Role created successfully!" );
  relocate( "roles?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.role = getInstance( "Role" ).findOrFail(rc.roleName);
  param prc.errors = flash.get( "role_form_errors", {} ); 

  event.setView( "roles/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
       // "roleName" : { "required" : true },
				"roleDescription" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "role_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var role = getInstance( "Role" ).findOrFail(rc.roleName);
  role.setRoleDescription( Ucase(rc.roleDescription) );
  role.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Role updated successfully!" );
  relocate( "roles.#role.getRoleName()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var role = getInstance( "Role" ).findOrFail(rc.roleName);

  try {

  role.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Role deleted successfully!" );
  relocate( "roles?seqID=#rc.seqID#" );

  }


}