/**
 * I am a new handler
 */
component secured {

property name="flash" inject="coldbox:flash";
property name="auth" inject="AuthenticationService@cbauth";
property name="messagebox" inject="MessageBox@cbmessagebox";  


function edit(event, rc, prc) {

rc.userID = auth.getUserID();	  
prc.user = getInstance( "User" ).findOrFail(rc.userID);

param prc.errors = flash.get( "myAccount_form_errors", {} );

// Retrieve all locales except the one already in the user preferences

prc.localefilter = prc.user.getLocaleCode();

prc.locale = getInstance( "Locale" ).findOrFail(prc.localeFilter);
prc.localefilterName = prc.locale.getLocaleName();

// Retrieve all locales except the one already in the user preferences

prc.filterLocalesExcept = getInstance( "Locale" )
.where( "localeCD","<>", prc.localefilter )
.get();



event.setView( "myAccount/edit");

}

function update(event, rc, prc) {

var result = validateModel(
		target = rc,
		constraints = {
		"firstname" : { "required" : true },
		"lastname" : { "required" : true },	
		"localeCode" : { "required" : true }
		}
	);

	if ( result.hasErrors() ) {
		flash.put( "myAccount_form_errors", result.getAllErrorsAsStruct() );
		redirectBack();
		return;
	}  

try {  

	rc.userID = auth.getUserID();
	var user = getInstance( "User" ).findOrFail(rc.userID);

	user.setFirstname(rc.firstname);
	user.setLastname(rc.lastname);
  	user.setLocaleCode(rc.localeCode);
  	user.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Account updated successfully!" );

  relocate( "myAccount.edit?seqID=#rc.seqID#" );

  }	


}
