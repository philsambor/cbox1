/**
* I am the Processes handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  prc.processes = getInstance( "Process" )
    .orderBy( "processCD" )
    .get();

  event.setView( "processes/index" );      

  }

 function show(event, rc, prc)  {

  prc.process = getInstance( "Process" ).findOrFail(rc.processCD);
  event.setView( "processes/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "process_form_errors", {} );

  // Retrieve all dictionary keys already assigned to processes and store them to an array

  var myArray = ArrayNew();
  myArray = getInstance( "Process" ).values( "processDictionaryKey" );

  // Dictionary group ID = 3 is exclusively holding process dictionary keys
  var groupID = 3;

  // Retrieve unassigned process dictionary keys for drop-down select 
  
  prc.processKeys = getInstance( "DictionaryKey").availableKeys(myArray, groupID);

  if (NOT arraylen(prc.processKeys)) {

		// Return the warning message
		messagebox.setMessage( type = "warn", message = "No available dictionary keys - Create at least one process dictionary key for assignment to a new process" );

    };                

  event.setView( "processes/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "processCD" : { "required" : true },
				//"processDictionaryKey" : { "required" : true },
        "processDescription" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "process_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var process = getInstance( "Process" ).create( {
    "processCD" : Ucase(rc.processCD),
    "processDictionaryKey" : Ucase(rc.processDictionaryKey),
    "processDescription" : Ucase(rc.processDescription),
    "processIcon" : Lcase(rc.processIcon)

  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Process created successfully!" );
  relocate( "processes?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) secured {
  prc.process = getInstance( "Process" ).findOrFail(rc.processCD);
  param prc.errors = flash.get( "process_form_errors", {} ); 

  // These data are needed to populate the drop-down select object in the view
  prc.dictionaryKeyfilter = prc.process.getProcessDictionaryKey();

  // Retrieve all data for the select drop-down except the one used as a filter
  prc.filterDictionaryKeys = getInstance( "DictionaryKey" )
    .where( "dictionaryKeyString","<>", prc.dictionaryKeyfilter )
    .get();

  event.setView( "processes/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "processCD" : { "required" : true },
				//"processDictionaryKey" : { "required" : true },
        "processDescription" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "process_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var process = getInstance( "Process" ).findOrFail(rc.processCD);
  process.setProcessDictionaryKey( Ucase(rc.processDictionaryKey) );
  process.setProcessDescription( Ucase(rc.processDescription) );
  process.setProcessIcon( Lcase(rc.processIcon) );
  process.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Process updated successfully!" );
  relocate( "processes.#process.getProcessCD()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var process = getInstance( "Process" ).findOrFail(rc.processCD);

  try {

  process.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Process deleted successfully!" );
  relocate( "processes?seqID=#rc.seqID#" );

  }


}