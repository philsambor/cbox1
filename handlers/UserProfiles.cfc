/**
* I am the user profiles handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  prc.userProfiles = getInstance( "UserProfile" ).all();

  event.setView( "userProfiles/index");

  }

 function show(event, rc, prc)  {

  prc.userProfile = getInstance( "UserProfile" ).findOrFail([rc.userLoginID, rc.userProfileID]);
  event.setView( "userProfiles/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "userProfile_form_errors", {} ); 

  // Any new profile associated to a user is assigned to that user, by default
	event.paramValue("userProfileOK",1);

  // Get data for the view's drop-down select controls
  prc.profiles = getInstance( "Profile" ).all();
  prc.users = getInstance( "User" ).all();

  event.setView( "userProfiles/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
	      "userWorkgroupID" : { "required" : true },
	      "UserProfileOK" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "userProfile_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var userProfile = getInstance( "UserProfile" ).create( {
    "userLoginID" : rc.userLoginID, 
    "userProfileID" : rc.userProfileID,
    "userWorkgroupID" : rc.userWorkgroupID,
    "userProfileOK" : rc.userProfileOK
  });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "User profile created successfully!" );
  relocate( "userProfiles?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {

  prc.userProfile = getInstance( "UserProfile" ).findOrFail([rc.userLoginID, rc.userProfileID]);
  param prc.errors = flash.get( "userProfile_form_errors", {} ); 

  // Get instance parameter values to manage UI controls on the view
  prc.userProfileOK = prc.userProfile.getUserProfileOK();

  // These data are needed to populate the drop-down select object in the view
  prc.filterUser = prc.userProfile.getUserLoginID();
  prc.filterUserEmail = prc.userProfile.getUser().getEmail();
  prc.filterProfile = prc.userProfile.getUserProfileID();
  prc.filterProfileName = prc.userProfile.getProfile().getProfileName();

  event.setView( "userProfiles/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "userWorkgroupID" : { "required" : true },
	      "UserProfileOK" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "userProfile_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var UserProfile = getInstance( "UserProfile" ).findOrFail([rc.userLoginID, rc.userProfileID]);
  userProfile.setUserLoginID( rc.userLoginID );
  userProfile.setUserProfileID( rc.userProfileID );
  userProfile.setUserWorkgroupID( rc.userWorkgroupID );
  userProfile.setUserProfileOK( rc.userProfileOK );
  userProfile.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "User profile updated successfully!" );
  relocate( "userProfiles.#userProfile.getUserLoginID()#.#userProfile.getUserProfileID()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var userProfile = getInstance( "UserProfile" ).findOrFail([rc.userLoginID, rc.userProfileID]);

  try {

  userProfile.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "User profile deleted successfully!" );
  relocate( "userProfiles?seqID=#rc.seqID#" );

  }


}