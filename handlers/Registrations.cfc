component {

	property name="str" inject="models/utils/RandomStringSVC";
	property name="auth" inject="AuthenticationService@cbauth";
	property name="flash" inject="coldbox:flash";

	function new( event, rc, prc ){
		param prc.errors = flash.get( "registration_form_errors", {} );
		event.setView( "registrations/new" );
	}

	function create( event, rc, prc ){

		// Build a unique temporary nickname concatenating a random string and the highest ID 
		// allocated to any user in the table

		var maxID = getInstance("User").max("id");

		// The same query with qb accessing tables instead of entities
		// query = wirebox.getInstance('QueryBuilder@qb');
		// var maxID = query.from( "tab_user_accounts" ).max( "id" );

		var rString = str.randstring(5);

		prc.nickname = '#rString#'&'#maxID#';

		var result = validateModel(
			target = rc,
			constraints = {
				"email" : {
					"required" : true,
					"type" : "email",
					"uniqueInDatabase" : { "table" : "tab_user_accounts", "column" : "usrla_email_login" }
				},
				"password" : { "required" : true },
				"passwordConfirmation" : { "required" : true, "sameAs" : "password" },
				"nickname" : {
					"uniqueInDatabase" : { "table" : "tab_user_accounts", "column" : "usrla_nickname" }
				},
				"firstname" : { "required" : true },
				"lastname" : { "required" : true },
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "registration_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}

		var user = getInstance( "User" ).create( { "email" : rc.email, 
												   "password" : rc.password,
												   "emailNotify" : rc.email,
												   "nickname" : prc.nickname,
												   "firstname": rc.firstname,
												   "lastname" : rc.lastname}
												);

		// login and redirect the user

		auth.login( user );

		relocate( uri = "/" );

	}

}
