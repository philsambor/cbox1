/**
* I am the Profiles handler object
*/

component secured { 

property name="flash" inject="coldbox:flash";
property name="messagebox" inject="MessageBox@cbmessagebox";  

  function index(event, rc, prc)  {

  // prc.profiles = getInstance( "Profile" ).all();

    prc.profiles = getInstance( "Profile" )
      .orderBy( "ProfileProcessCD" )
      .get(); 

    event.setView( "profiles/index");   

    }

 function show(event, rc, prc)  {

  prc.profile = getInstance( "Profile" ).findOrFail(rc.ProfileID);

  event.setView( "profiles/show");

  }

  function new(event, rc, prc) {
  param prc.errors = flash.get( "profile_form_errors", {} ); 

  // Get data for the view's drop-down select controls
  prc.processes = getInstance( "Process" ).all();
  prc.roles = getInstance( "Role" ).all();
  prc.profileScopes = getInstance( "ProfileScope" ).all();

  event.setView( "profiles/new");

  }

  function create(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "profileName" : { "required" : true },
	      //"profileProcessCD" : { "required" : true },
	      // "profileRoleName" : { "required" : true },
        // "profileScopeID" : { "required" : true }
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "profile_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		} 

  try {

    var Profile = getInstance( "Profile" ).create( {
    "profileName" : Ucase(rc.ProfileName),
    "profileProcessCD" : Ucase(rc.profileProcessCD),
    "profileRoleName" : Ucase(rc.ProfileRoleName),
    "profileScopeID" : rc.ProfileScopeID,
    "profileDescription" : Ucase(rc.ProfileDescription)
    });  

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Profile created successfully!" );
  relocate( "profiles?seqID=#rc.seqID#" );

  }

  function edit(event, rc, prc) {
  prc.profile = getInstance( "Profile" ).findOrFail(rc.profileID);
  param prc.errors = flash.get( "profile_form_errors", {} );

  // These data are needed to populate the drop-down select object in the view
  prc.filterProcess = prc.profile.getProfileProcessCD();
  prc.filterProcessName = prc.profile.getProcess().getProcessDescription();
  prc.filterScope = prc.profile.getProfileScopeID();
  prc.filterScopeName = prc.profile.getProfileScope().getProfileScopeName();
  prc.filterRole = prc.profile.getProfileRoleName();
  prc.filterRoleDescription = prc.profile.getRole().getRoleDescription();

  // Retrieve all data for the select drop-down except the one used as a filter
  
  prc.filterProfileScopes = getInstance( "ProfileScope" )
    .where( "profileScopeID","<>", prc.filterScope )
    .get();

   prc.filterRoles = getInstance( "Role" )
    .where( "roleName","<>", prc.filterRole )
    .get();  

  event.setView( "profiles/edit");

  }

  function update(event, rc, prc) {

  var result = validateModel(
			target = rc,
			constraints = {
        "profileName" : { "required" : true },
        //"profileProcessCD" : { "required" : true },
	      //"profileRoleName" : { "required" : true },
        //"profileScopeID" : { "required" : true }   
			}
		);

		if ( result.hasErrors() ) {
			flash.put( "profile_form_errors", result.getAllErrorsAsStruct() );
			redirectBack();
			return;
		}  

  try {  

  var profile = getInstance( "Profile" ).findOrFail(rc.ProfileID);
  profile.setProfileName( Ucase(rc.profileName) );
  profile.setProfileProcessCD( Ucase(rc.profileProcessCD) );
  profile.setProfileRoleName( Ucase(rc.profileRoleName) );
  profile.setProfileScopeID( rc.profileScopeID );
  profile.setProfileDescription( Ucase(rc.profileDescription) );
  profile.save();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Profile updated successfully!" );
  relocate( "profiles.#profile.getProfileID()#?seqID=#rc.seqID#" );

  }

  function delete(event, rc, prc) {
  var profile = getInstance( "Profile" ).findOrFail(rc.profileID);

  try {

  profile.delete();

  } catch(database e) {

    messagebox.setMessage( type = "error", message = "#e.message#" );
    redirectBack();
		return;

    }

  messagebox.setMessage( type = "success", message = "Profile deleted successfully!" );
  relocate( "profiles?seqID=#rc.seqID#" );

  }


}