component secured {
	
	// Dependency Injection

	property name="menu" inject="models.MenuContentSVC";
	property name="messagebox" inject="MessageBox@cbmessagebox";
	property name="tenantCompany" inject="TenantCompanySVC";

	function sessionview(event,rc,prc,seqID){	

	// Get the database default date and time storage format
	prc.dbDatetime = tenantCompany.getTenantdbDatetimeFormat();
	prc.dbDate = tenantCompany.getTenantdbDateFormat();

	try {

	// Update the navigation cursor
	prc.seqID = arguments.seqID;

	// Retrieve the navigation structure
	prc.structNav = menu.getAuthNavigation();
	prc.arrayAppSubmenu = menu.buildAppSubmenu(prc.seqID);

	// Ensure URL route integrity

	} catch (e) {

	relocate( uri = "/" );	

	}
	
	//writedump(var = rc);

	// if user session is timed out
	if (auth().guest()) {

		relocate( uri = "/" );

		} else {

		// Render the new event view		
		return renderview("viewlets/sessionview");

		}

	}

	function routeNotFound(event,rc,prc){

    // Set the error page and render a 404 header
	event.setView( "main/error404" ).setHTTPHeader( "404", "Page Not Found" );
	
	} 
	
}

