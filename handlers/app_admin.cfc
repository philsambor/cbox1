/**
* I am Admin app handler
*/
component{

	property name="menu" inject="models.MenuContentSVC";
		
	/**
	* index
	*/
	
	function index( event, rc, prc ){
		
		prc.structNav = menu.getAuthNavigation();
		event.setView( "app_admin/index" );
	}


	
}
